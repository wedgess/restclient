package eu.wedgess.restclient.fragments;

import java.util.List;

import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.model.RequestModel;

/**
 * Subclass of {@link BaseStoredDataFragment} simply returns the list of items from the model which
 * are for this fragment (Saved Items) to the super.
 */
public class SavedRequestFragment extends BaseStoredDataFragment {

    /**
     * Return the list of saved items for the super class
     *
     * @return - adapters list for {@link BaseStoredDataFragment}
     */
    @Override
    List<RequestData> getAdapterList() {
        return RequestModel.getInstance().getSavedRequestDataList();
    }
}
