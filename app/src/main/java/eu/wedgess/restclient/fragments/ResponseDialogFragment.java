package eu.wedgess.restclient.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.adapters.ResponseHeaderAdapter;
import eu.wedgess.restclient.model.ResponseData;
import eu.wedgess.restclient.tasks.SyntaxHighlighterTask;
import eu.wedgess.restclient.utils.Constants;
import eu.wedgess.restclient.utils.SimpleDividerItemDecoration;

/**
 * Use a dialog fragment to wrap around the AlertDialog, reason being is that
 * on orientation change dialog fragemnt will stay displayed if already open
 * an alert dialog however closes after orientation change.
 * <p>
 * Created by Gareth on 30/11/2016.
 */

public class ResponseDialogFragment extends DialogFragment {

    public static final String TAG = ResponseDialogFragment.class.getSimpleName();

    private TextView mResponseStatusCodeTV, mResponseBodyTV, mResponseTimeTV;
    private RecyclerView mRecyclerViewHeaders;
    private ImageButton mResponseHeadersExpandIB, mResponseBodyExpandIB;
    private ResponseData mResponseData;

    public ResponseDialogFragment() {
        //default constructor
    }

    /**
     * Returns a new instance of this fragment, this method should be called when
     * creating a ResponseDialogFragment. DO NOT USE default constructor which needs to be added.
     *
     * @param responseData - the response data to be sent
     * @return - the instance of this fragment with arguments set
     */
    public static ResponseDialogFragment newInstance(ResponseData responseData) {
        // set the parcelable as the arguments for this fragment
        Bundle args = new Bundle();
        args.putParcelable(TAG, responseData);

        ResponseDialogFragment fragment = new ResponseDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // don't need to restore instance state for ResponseData as it does not change
        if (getArguments() != null) {
            mResponseData = getArguments().getParcelable(TAG);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // check if headers and body are shown or not
        outState.putBoolean(Constants.SAVED_STATE_RESPONSE_IS_HEADERS_EXPANDED, mResponseHeadersExpandIB.getRotation() == -180f);
        outState.putBoolean(Constants.SAVED_STATE_RESPONSE_IS_BODY_EXPANDED, mResponseBodyExpandIB.getRotation() == 0f);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View customView = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_response, null);

        // reference the widgets within the layout
        mRecyclerViewHeaders = (RecyclerView) customView.findViewById(R.id.recycler_response_headers);
        mRecyclerViewHeaders.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        mResponseStatusCodeTV = (TextView) customView.findViewById(R.id.tv_status_code);
        mResponseBodyTV = (TextView) customView.findViewById(R.id.tv_response_body);
        mResponseTimeTV = (TextView) customView.findViewById(R.id.tv_response_time);
        mResponseHeadersExpandIB = (ImageButton) customView.findViewById(R.id.ib_expand_response_headers);
        mResponseBodyExpandIB = (ImageButton) customView.findViewById(R.id.ib_expand_response_body);

        // set click listeners on the expand more/less image buttons
        mResponseHeadersExpandIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandHeadersUI(mRecyclerViewHeaders.getVisibility() == View.GONE);
            }
        });
        mResponseBodyExpandIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandBodyUI(mResponseBodyTV.getVisibility() == View.GONE);
            }
        });

        setData(mResponseData);
        // expand views or not depending on previous state before orientation change
        if (savedInstanceState != null) {
            expandHeadersUI(savedInstanceState.getBoolean(Constants.SAVED_STATE_RESPONSE_IS_HEADERS_EXPANDED));
            expandBodyUI(savedInstanceState.getBoolean(Constants.SAVED_STATE_RESPONSE_IS_BODY_EXPANDED));
        }

        // use an alert dialog and set the customView as its view
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.fragment_title_response)
                .setView(customView)
                .setCancelable(false)
                .setNegativeButton(R.string.btn_close,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();

        return alertDialog;
    }


    // used for expand/less image buttons to hide/show headers used on orientation change and on button click
    private void expandHeadersUI(boolean expanded) {
        if (expanded) {
            mResponseHeadersExpandIB.animate().rotation(-180f).setDuration(200);
            mRecyclerViewHeaders.setVisibility(View.VISIBLE);
        } else {
            mResponseHeadersExpandIB.animate().rotation(0f).setDuration(200);
            mRecyclerViewHeaders.setVisibility(View.GONE);
        }
    }

    // used for expand/less image buttons to hide/show body used on orientation change and on button click
    private void expandBodyUI(boolean expanded) {
        if (expanded) {
            mResponseBodyTV.setVisibility(View.VISIBLE);
            mResponseBodyExpandIB.animate().rotation(0f).setDuration(200);
        } else {
            mResponseBodyTV.setVisibility(View.GONE);
            mResponseBodyExpandIB.animate().rotation(180f).setDuration(200);
        }
    }

    /**
     * Set the data on UI elements of passed ResponseData
     */
    private void setData(ResponseData responseData) {
        // not sure why but ResponseData can be null after app was in the background for a long time
        if (responseData != null) {
            if (mResponseStatusCodeTV != null) {
                mResponseStatusCodeTV.setText(String.valueOf(responseData.getStatusCode()));
            }
            if (mResponseTimeTV != null) {
                mResponseTimeTV.setText(String.valueOf(responseData.getTimeTaken()));
            }
            if (mResponseBodyTV != null) {
                // response body can be null or empty, for example if status is 403 etc
                if (responseData.getBody() != null && !TextUtils.isEmpty(responseData.getBody())) {
                    // Convert to a JSON object, will cause an exception if not valid JSON
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(responseData.getBody());
                        mResponseBodyTV.setText(jsonObject.toString(4));
                    } catch (JSONException e) {
                        // in case of an exception (not JSON) just set the body text
                        mResponseBodyTV.setText(responseData.getBody());
                    }
                }
                // set the status code badges color
                setResponseStatusColor();
                // syntax highlight response body
                startSyntaxHighlighter();
            }
            // set headers adapter
            ResponseHeaderAdapter mAdapter = new ResponseHeaderAdapter(responseData.getHeaders());
            if (mRecyclerViewHeaders != null) {
                mRecyclerViewHeaders.setAdapter(mAdapter);
                if (TextUtils.isEmpty(mResponseBodyTV.getText())) {
                    expandHeadersUI(true);
                }
            }
        }
    }


    // start syntax highlighter AsyncTask on the response body text in the background
    private void startSyntaxHighlighter() {
        new SyntaxHighlighterTask(new SyntaxHighlighterTask.HighlightCompleteCallback() {
            @Override
            public void onComplete(Editable e) {
                // set the result in the response body -- now syntax highlighted
                mResponseBodyTV.setText(e);
                //Log.i(TAG, "Task completed");
            }
        }, getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mResponseBodyTV.getText().toString(), "");
    }

    // set the response status code TextViews color depending on first character eg. 1,2,3,4,5
    private void setResponseStatusColor() {
        Drawable drawable = mResponseStatusCodeTV.getBackground();
        if (!TextUtils.isEmpty(mResponseStatusCodeTV.getText()) && drawable != null) {
            switch (mResponseStatusCodeTV.getText().charAt(0)) {
                case '1': // information - blue
                    drawable.setColorFilter(Color.rgb(33, 150, 243), PorterDuff.Mode.MULTIPLY);
                    break;
                case '2': // success - green
                    drawable.setColorFilter(Color.rgb(76, 175, 80), PorterDuff.Mode.MULTIPLY);
                    break;
                case '3': // redirection - yellow
                    drawable.setColorFilter(Color.rgb(255, 235, 59), PorterDuff.Mode.MULTIPLY);
                    break;
                case '4': // Client error
                case '5': // Server error - red
                default: // error
                    drawable.setColorFilter(Color.rgb(244, 67, 54), PorterDuff.Mode.MULTIPLY);
                    break;
            }
        }
    }

    /*
     * get the response data from the arguments as it will always be set,
     * if I attempt to get it from the mResponseData it can be empty after 2
     * orientation changes without re-opening the dialog.
     *
     * Getting the responseData from the arguments fixes this.
     */
    public ResponseData getResponseData() {
        return getArguments().getParcelable(TAG);
    }
}
