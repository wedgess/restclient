package eu.wedgess.restclient.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.activities.MainViewPagerActivity;
import eu.wedgess.restclient.activities.RequestActivity;
import eu.wedgess.restclient.adapters.ExtendedAdapter;
import eu.wedgess.restclient.adapters.SelectableAdapter;
import eu.wedgess.restclient.adapters.StoredDataAdapter;
import eu.wedgess.restclient.db.DBUtils;
import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.model.RequestModel;
import eu.wedgess.restclient.utils.Constants;
import eu.wedgess.restclient.utils.RecyclerItemClickListener;
import eu.wedgess.restclient.utils.SimpleDividerItemDecoration;

/**
 * Classes extending this class are {@link HistoryRequestFragment} & {@link SavedRequestFragment}
 * both override the this classes on create, one to add arecycler scroll listener to hide mAddRequestFAB on scroll
 * and the other sets the empty layout values.
 */
public abstract class BaseStoredDataFragment extends Fragment implements RecyclerItemClickListener,
        ExtendedAdapter.OnDeleteCompleteListener,
        ActionMode.Callback {

    private static final String TAG = MainViewPagerActivity.class.getSimpleName();
    private TextView mEmptyView;
    private CoordinatorLayout mCoOrdinatorLayout;
    private RecyclerView mRecyclerView;
    private StoredDataAdapter mAdapter;
    private ActionMode mActionMode;
    private Snackbar mUndoSnackbar;
    private FloatingActionButton mAddRequestFAB;

    /**
     * Either returns {@link RequestModel} classes saved list or history list
     *
     * @return list to save into adapter
     */
    abstract List<RequestData> getAdapterList();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set options menu to show seed DB with test data option
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stored_data, container, false);

        mCoOrdinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.coordinator_history);
        mEmptyView = (TextView) rootView.findViewById(R.id.tv_empty_view);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_stored_items);

        final boolean isSavedFragment = this instanceof SavedRequestFragment;
        // if this is the SavedRequestFragment
        if (isSavedFragment) {
            // default image is for HistoryRequestFragment, so set saved items empty text and image for saved request
            mEmptyView.setCompoundDrawablesWithIntrinsicBounds(null, ContextCompat.getDrawable(getActivity(),
                    R.drawable.ic_empty_saved_items), null, null);
            mEmptyView.setText(getString(R.string.empty_layout_msg_saved));
        } else {
            // is HistoryRequestFragment so show FAB
            mAddRequestFAB = (FloatingActionButton) rootView.findViewById(R.id.fab_add_request);
            mAddRequestFAB.setVisibility(View.VISIBLE);
            mAddRequestFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), RequestActivity.class));
                }
            });
        }

        // add line divider for items in RecyclerView
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        //Log.i(TAG, "ListSize: " + getAdapterList().size());
        // initialize the Adapter and set to RecyclerView
        mAdapter = new StoredDataAdapter(getAdapterList(), isSavedFragment, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        /*
         * if screen orientation has changed etc.. then call {@link SelectableAdapter#onRestoreInstanceState(Bundle)}  }
         * to set selection and mode, if mode is multi then restart actionmode and set its title.
         */
        if (savedInstanceState != null) {
            mAdapter.onRestoreInstanceState(savedInstanceState);
            if (mAdapter.getMode() == SelectableAdapter.MODE_MULTI) {
                mActionMode = getActivity().startActionMode(this);
                mActionMode.setTitle(getString(R.string.action_title_items_selected,
                        mAdapter.getSelectedItemCount()));
            }
        }
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.seed_db) {
            // add predefined request items to the DB
            RequestModel.getInstance().seedDBData(getActivity());
            Snackbar.make(mCoOrdinatorLayout, R.string.snackbar_message_seeded_db, Snackbar.LENGTH_LONG).show();
            // reset items in adapter
            mAdapter.notifyDataSetChanged();
            // if empty layout was showing then show the RecyclerView as items are not present in adapter
            toggleEmptyLayout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Toggle the selection state of an item.
     * <p/>
     * If the item was the last one in the selection and is unselected, the selection is
     * stopped (returns to MODE_SINGLE).
     * Note: selection must already be started (actionMode must not be null).
     * {@link SelectableAdapter} to animate change
     *
     * @param position Position of the item to toggle the selection state
     */
    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);

        int count = mAdapter.getSelectedItemCount();
        // if no items are selected finish ActionMode
        if (count == 0) {
            mActionMode.finish();
        } else {
            // otherwise update ActionModes title to the new number of selected items
            mActionMode.setTitle(getString(R.string.action_title_items_selected, count));
            //mActionMode.invalidate();
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_context_history, menu);
        // Actionmode created so set Adapters mode to MULTI_SELECT
        mAdapter.setMode(StoredDataAdapter.MODE_MULTI);
        // if the FAB is not null then hide it, won't be null if it is the HistoryRequestFragment
        if (mAddRequestFAB != null) {
            mAddRequestFAB.hide();
        }
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        boolean canCloseActionMode = false;
        switch (item.getItemId()) {
            case R.id.action_delete:
                canCloseActionMode = true;
                // remove selected items from just the adapter
                mAdapter.removeItems(mAdapter.getSelectedItems());
                // get number of deleted items to display in snack bars text
                int count = mAdapter.getDeletedItemCount();
                // depending on item count show deleted <n> item or deleted <n> items
                mUndoSnackbar = Snackbar.make(mCoOrdinatorLayout, getString(
                        count > 1 ? R.string.snackbar_message_deleted_items : R.string.snackbar_message_deleted_item, count),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.action_undo, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // In UNDO action to restore deleted items
                                mAdapter.restoreDeletedItems();
                                // toggle empty layout in case all items were deleted
                                // then restored,  the recycler needs to be View.VISIBLE
                                toggleEmptyLayout();
                            }
                        });
                mUndoSnackbar.show();
                mAdapter.startUndoTimer(5000, this); // start undo timer for 5 seconds
                // toggle empty layout as all items maybe deleted
                toggleEmptyLayout();
                break;
            case R.id.action_select_all:
                // if all items are already selected then clear the selection, otherwise select all
                if (mAdapter.getSelectedItemCount() == mAdapter.getItemCount()) {
                    canCloseActionMode = true;
                    mAdapter.clearSelection();
                } else {
                    mAdapter.selectAll();
                    mActionMode.setTitle(getString(R.string.action_title_items_selected,
                            mAdapter.getSelectedItemCount()));
                }
                break;
        }
        // close the actionmode only if no items are selected
        if (mActionMode != null && canCloseActionMode) {
            mActionMode.finish();
        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        // reset after mode to single as the action mode is being destroyed
        mAdapter.setMode(StoredDataAdapter.MODE_SINGLE);
        mAdapter.clearSelection();
        mActionMode = null;
        // if NOT null then it is the HistoryRequestFragment ,so reshow FAB
        if (mAddRequestFAB != null) {
            mAddRequestFAB.show();
        }
    }


    /**
     * Implemented method from interface:
     * {@link eu.wedgess.restclient.adapters.ExtendedAdapter.OnDeleteCompleteListener}
     * called when undo timer time has ran out and items need to be deleted from the DB.
     */
    @Override
    public void onDeleteConfirmed() {
        // dismiss snackbar if snackbar isn't null(has been null on orientation change) and is shown
        if (mUndoSnackbar != null && mUndoSnackbar.isShown()) {
            mUndoSnackbar.dismiss();
        }
        // delete each item in the DeletedItems List when the undo timer has run out
        for (RequestData requestData : mAdapter.getDeletedItems()) {
            DBUtils.deleteRequestById(requestData.getId(), getActivity());
        }
    }

    /**
     * Implemented method from interface: {@link RecyclerItemClickListener}
     * this gets the position of an item clicked event in RecyclerView.
     *
     * @param position - position of item clicked.
     */
    @Override
    public void onItemClick(int position) {
        // if in multi-select and position is not -1, toggle selection on item, otherwise view item
        if (mActionMode != null && position != Constants.INVALID_POSITION) {
            toggleSelection(position);
        } else {
            /**
             * {@link RequestData} is {@link android.os.Parcelable} therefore can send the RequestData
             * item to the next fragment.
             */
            Intent intent = new Intent(getActivity(), RequestActivity.class);
            intent.putExtra(Constants.INTENT_EXTRA_PASSED_REQUEST, getAdapterList().get(position));
            startActivity(intent);
        }
    }

    /**
     * Implemented method from interface: {@link RecyclerItemClickListener}
     * this gets the position of an items long click event in RecyclerView.
     *
     * @param position - position of item clicked.
     */
    @Override
    public void onItemLongClick(int position) {
        // if actionmode is not started then start it.
        if (mActionMode == null) {
            mActionMode = getActivity().startActionMode(this);
        }
        // toggle the selection of the item at position
        toggleSelection(position);
    }

    /**
     * Toggle for RecyclerView -- Empty TextView
     * Hides RecyclerView if there are no items and displays a TextView with
     * image. If there are items Empty TextView is hidden.
     */
    private void toggleEmptyLayout() {
        if (mAdapter != null) {
            // if adapters empty show empty layout
            if (mAdapter.getAllItems().isEmpty()) {
                mRecyclerView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
            } else if (mRecyclerView.getVisibility() == View.GONE) {
                // if recycler is hidden and list is not empty reshow recycler
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // initially show empty layout or RecyclerView with the items depending on if list is empty or not
        if (mAdapter != null) {
            // if returning to the application after a long time the adapters list can be null
            // if the list in null ignore this as it will be rebuilt in onCreate().
            if (mAdapter.getAllItems() != null) {
                // this helps for when returning to the activity from the request activity that list
                // items are updates in the adapter
                mAdapter.notifyDataSetChanged();
                toggleEmptyLayout();
            }
        }
    }
}
