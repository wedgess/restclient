package eu.wedgess.restclient.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.adapters.ExtendedAdapter;
import eu.wedgess.restclient.adapters.StoredDataAdapter;
import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.model.RequestModel;

/**
 * Subclass of {@link BaseStoredDataFragment} used to display list of history request items
 * in a RecyclerView.
 * <p>
 * Created by Gareth on 11/11/2016.
 */

public class HistoryRequestFragment extends BaseStoredDataFragment {

    //private static final String TAG = MainViewPagerActivity.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // get the rootView from the super class
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        // get recycler and fab from supers layout so we can add scroll listener to RecyclerView
        final RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_stored_items);
        final FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab_add_request);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // if scrolling and fab is showing then hide it
                if (dy > 0 || dy < 0 && fab.isShown()) {
                    fab.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // recyclers adapter is always going to be of type StoredDataAdapter
                // so check its mode before reshowing the FAB.
                if (newState == RecyclerView.SCROLL_STATE_IDLE
                        && ((StoredDataAdapter) recyclerView.getAdapter()).getMode() != ExtendedAdapter.MODE_MULTI) {
                    fab.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        // return supers view with the recycler scroll listener
        return rootView;
    }

    /**
     * Returns history adapter items for super class
     *
     * @return the adapter list for {@link BaseStoredDataFragment}
     */
    @Override
    List<RequestData> getAdapterList() {
        return RequestModel.getInstance().getHistoryRequestDataList();
    }
}
