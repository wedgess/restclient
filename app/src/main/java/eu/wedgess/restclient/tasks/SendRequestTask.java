package eu.wedgess.restclient.tasks;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.model.ResponseData;
import eu.wedgess.restclient.utils.Constants;
import eu.wedgess.restclient.utils.Utils;

/**
 * AsyncTask which runs the Request in a background thread. Reason being is that Network
 * work cannot be done on the main thread (UI thread).
 * Classes which use this SendRequestTask should implement {@link RequestCallback} interface or pass in an
 * anonymous listener through the constructor.
 * <p>
 * Created by Gareth on 12/11/2016.
 */

public class SendRequestTask extends AsyncTask<RequestData, ResponseData, ResponseData> {

    private static final String TAG = SendRequestTask.class.getSimpleName();
    private long mStartTime;
    private RequestCallback mRequestCallback;

    // constructor takes a RequestCallback instance
    public SendRequestTask(RequestCallback requestCallback) {
        this.mRequestCallback = requestCallback;
    }

    /**
     * Callback interface for when a Request has started, completed, timed out, or error occured
     * implemented within {@link eu.wedgess.restclient.activities.RequestActivity}
     */
    public interface RequestCallback {
        void onStart();

        void onConnectionTimeout();

        void onError(String error);

        void onComplete(ResponseData responseData);
    }

    @Override
    protected void onPreExecute() {
        // set start time and notify listener that task has started
        mStartTime = System.currentTimeMillis();
        if (mRequestCallback != null) {
            mRequestCallback.onStart();
        }
    }

    // return response data to onPostExecute
    @Override
    protected ResponseData doInBackground(RequestData... requestDatas) {
        return sendRequest(requestDatas[0]);
    }

    @Override
    protected void onPostExecute(final ResponseData responseData) {
        // will be null if connection timed out or IO exception
        if (responseData != null) {
            mRequestCallback.onComplete(responseData);
        }
    }

    private ResponseData sendRequest(final RequestData requestData) {
        BufferedReader reader = null;
        try {
            // build URL object from RequestData objects URL
            URL url = new URL(requestData.getUrl());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(requestData.getTimeOut());

            // set the request method, POST, GET... etc.
            con.setRequestMethod(requestData.getMethod());

            // set the request header data
            if (!requestData.getHeaders().isEmpty()) {
                for (Map.Entry<String, String> entry : requestData.getHeaders().entrySet()) {
                    //Log.i(TAG, "HEADERS --> Key: " + entry.getKey() + "  Value: " + entry.getValue());
                    con.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            // write the params to the body if it is a POST or PUT request
            if (requestData.getMethod().equals(Constants.REQUEST_METHOD_POST)
                    || requestData.getMethod().equals(Constants.REQUEST_METHOD_PUT)) {
                con.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                con.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
                // if the body params is not empty write them
                if (!requestData.getBodyKeyValuePair().isEmpty()) {
                    // set content type header
                    writer.write(requestData.getBodyKeyValueEncodedParams());
                    // otherwise check if bodyRaw is not empty, if not write it
                    //Log.i(TAG, "Writing body key/values");
                    // otherwise is the body raw is not empty write it to body of request
                } else if (!TextUtils.isEmpty(requestData.getBodyRaw())) {
                    //Log.i(TAG, "Writing body raw");
                    writer.write(requestData.getBodyRaw());
                }
                //Log.i(TAG, "Flushing Write");
                writer.flush();
            } else {
                con.setDoOutput(false);
            }

            // set at bottom in ResponseData
            int status = con.getResponseCode();
            //Log.i(TAG, "Setting Status Code: " + status);

            // StringBuilder for response
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String line;
            // while response line is not null -- for response body
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n"); // append line to StringBuilder
            }

            // get the header fields for response
            Map<String, List<String>> map = con.getHeaderFields();
            Map<String, String> responseHeaders = new HashMap<>();
            // for each entry set in the headers Map, add the key and value to the responseHeaders Map
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                // don;t want headers specific to android so filter out X-Android etc
                if (entry.getKey() != null && !entry.getKey().contains("X-Android")) {
                    responseHeaders.put(entry.getKey(), Utils.listToDelimitedString(entry.getValue()));
                }
            }

            // get time taken for response
            long elapsedTime = System.currentTimeMillis() - mStartTime;

            // return ResponseData object
            return new ResponseData(responseHeaders, status, elapsedTime, sb.toString());

        } catch (SocketTimeoutException e) {
            //e.printStackTrace();
            Log.e(TAG, "CONNECTION TIMEOUT");
            mRequestCallback.onConnectionTimeout();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IO ERROR: " + e.getMessage());
            mRequestCallback.onError(e.getMessage());
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    //return null;
                }
            }
        }
    }
}
