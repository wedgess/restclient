package eu.wedgess.restclient.tasks;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.wedgess.restclient.R;

/**
 * This AsyncTask syntax highlights the response body in the {@link eu.wedgess.restclient.fragments.ResponseDialogFragment}
 * It is run in a background thread so that there is not delay is displaying the text. Once the text has been
 * highlighted it is set in the TextView. Classes which use this must implement the {@link HighlightCompleteCallback}
 * <p>
 * Created by Gareth on 11/11/2016.
 */
public class SyntaxHighlighterTask extends AsyncTask<String, Void, Editable> {

    /**
     * Regular expression patterns for matching JSON, XML & HTML
     * TODO: Still not exactly correct but working for the most part.
     */
    // Strings
    private static final Pattern GENERAL_STRINGS = Pattern.compile("\"(.*?)\"|'(.*?)'");
    // JSON
    private static final Pattern JSON_KEY = Pattern.compile("(\\\".*\\\")\\s*(?=:)");
    private static final Pattern JSON_VALUE = Pattern.compile("(?!.*:)(\\\".*\\\")");
    private static final Pattern JSON_VALUE_DIGIT = Pattern.compile("(?!.*:)(\\d)");

    // Numbers & Symbols
    private static final Pattern NUMBERS = Pattern.compile(
            "(\\b(\\d*[.]?\\d+)\\b)");
    private static final Pattern SYMBOLS = Pattern.compile(
            "(!|,|\\(|\\)|\\+|\\-|\\*|<|>|=|\\.|\\?|;|\\{|\\}|\\[|\\]|\\|)");
    private static final Pattern NUMBERS_OR_SYMBOLS = Pattern.compile(NUMBERS.pattern() + "|" + SYMBOLS.pattern());

    // Comments
    private static final Pattern XML_COMMENTS = Pattern.compile("(?s)<!--.*?-->");
    private static final Pattern GENERAL_COMMENTS = Pattern.compile(
            "/\\*(?:.|[\\n\\r])*?\\*/|(?<!:)//.*|#.*");

    // same as GENERAL_COMMENTS but without -> //
    private static final Pattern GENERAL_COMMENTS_NO_SLASH = Pattern.compile(
            "/\\*(?:.|[\\n\\r])*?\\*/|#.*");


    // HTML/XML
    private static final Pattern HTML_TAGS = Pattern.compile(
            "<([A-Za-z][A-Za-z0-9]*)\\b[^>]*>|</([A-Za-z][A-Za-z0-9]*)\\b[^>]*>");
    private static final Pattern HTML_ATTRS = Pattern.compile(
            "(\\S+)=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?");
    // adapted from post by Phil Haack and modified to match better
    private final static String TAG_START =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)\\>";
    private final static String TAG_END =
            "\\</\\w+\\>";
    private final static String TAG_SELF_CLOSING =
            "\\<\\w+((\\s+\\w+(\\s*\\=\\s*(?:\".*?\"|'.*?'|[^'\"\\>\\s]+))?)+\\s*|\\s*)/\\>";
    private final static String HTML_ENTITY =
            "&[a-zA-Z][a-zA-Z0-9]+;";
    // combined HTML patterns
    private final static Pattern HTML_PATTERN = Pattern.compile(
            "(" + TAG_START + ".*" + TAG_END + ")|(" + TAG_SELF_CLOSING + ")|(" + HTML_ENTITY + ")",
            Pattern.DOTALL
    );

    private HighlightCompleteCallback mHighlightCompleteCallback;
    private Context mContext;

    /*
     * Callback for when the highlighting task is done, returns an editable
     * which is the highlighted text
     */
    public interface HighlightCompleteCallback {
        void onComplete(Editable e);
    }

    public SyntaxHighlighterTask(HighlightCompleteCallback highlightCompleteCallback, Context context) {
        this.mHighlightCompleteCallback = highlightCompleteCallback;
        this.mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected final Editable doInBackground(String... params) {
        return highlight(params[0]);
    }

    @Override
    protected void onPostExecute(Editable syntaxHighlightedText) {
        super.onPostExecute(syntaxHighlightedText);
        // pass back the syntax highlighted editable
        mHighlightCompleteCallback.onComplete(syntaxHighlightedText);
    }

    /**
     * Used to apply color to the editable based on if the pattern matches
     *
     * @param pattern - pattern to apply
     * @param allText - text to highlight
     */
    private void color(Pattern pattern,
                       Editable allText) {
        int color = 0;
        // if pattern matches the specific regex pattern then set the colcor based on the match
        if (pattern.equals(HTML_TAGS)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_keyword);
        } else if (pattern.equals(HTML_ATTRS)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_attr);
        } else if (pattern.equals(SYMBOLS)) {
            color = Color.BLACK;
        } else if (pattern.equals(XML_COMMENTS)
                || pattern.equals(GENERAL_COMMENTS)
                || pattern.equals(GENERAL_COMMENTS_NO_SLASH)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_comment);
        } else if (pattern.equals(GENERAL_STRINGS)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_string);
        } else if (pattern.equals(NUMBERS) || pattern.equals(NUMBERS_OR_SYMBOLS)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_number);
        } else if (pattern.equals(JSON_KEY)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_keyword);
        } else if (pattern.equals(JSON_VALUE) || pattern.equals(JSON_VALUE_DIGIT)) {
            color = ContextCompat.getColor(mContext, R.color.syntax_variable);
        }

        Matcher m = pattern.matcher(allText);
        // while there is a match apply the Color from start to end of match
        while (m.find()) {
            allText.setSpan(
                    new ForegroundColorSpan(color),
                    m.start(),
                    m.end(),
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    /**
     * Used to build the Editable which is syntax highlighted
     *
     * @param text - string to syntax highlight
     * @return - syntax highlighted Editable
     */
    private Editable highlight(String text) {
        Editable editable = new SpannableStringBuilder(text);
        editable.clearSpans();

        if (editable.length() == 0) {
            return editable;
        }

        // HTML and XML
        if (isHtml(text)) {
            color(HTML_TAGS, editable);
            color(HTML_ATTRS, editable);
            color(GENERAL_STRINGS, editable);
            color(XML_COMMENTS, editable);
        } else if (isValidJSON(text)) { // If JSON
            color(JSON_KEY, editable);
            color(JSON_VALUE, editable);
            color(JSON_VALUE_DIGIT, editable);
        }

        return editable;
    }

    /**
     * Check if string is valid JSON, exception will be thrown in not JSON
     *
     * @param test - string to test if its JSON
     * @return - if valid JSON or not
     */
    private boolean isValidJSON(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Will return true if s contains HTML markup tags or entities.
     *
     * @param string String to test
     * @return true if string contains HTML
     */
    private boolean isHtml(String string) {
        boolean ret = false;
        if (string != null) {
            ret = HTML_PATTERN.matcher(string).find();
        }
        return ret;
    }
}
