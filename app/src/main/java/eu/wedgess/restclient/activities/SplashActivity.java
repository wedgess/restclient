package eu.wedgess.restclient.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import eu.wedgess.restclient.model.RequestModel;

/**
 * A splash screen which initializes the data in the {@link RequestModel} before starting
 * the {@link MainViewPagerActivity}.
 * <p>
 * Created by Gareth on 01/12/2016.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize models data
        RequestModel.getInstance().initializeData(this);
        Intent intent = new Intent(this, MainViewPagerActivity.class);
        // clear activities
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // start MainViewPager activity
        startActivity(intent);
        // finish this activity
        finish();
    }
}
