package eu.wedgess.restclient.activities;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.db.DBUtils;
import eu.wedgess.restclient.fragments.ResponseDialogFragment;
import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.model.RequestModel;
import eu.wedgess.restclient.model.ResponseData;
import eu.wedgess.restclient.tasks.SendRequestTask;
import eu.wedgess.restclient.utils.Constants;
import eu.wedgess.restclient.utils.LinearLayoutSerialized;
import eu.wedgess.restclient.utils.Utils;

/**
 * Created by Gareth on 11/11/2016.
 **/
public class RequestActivity extends AppCompatActivity implements View.OnClickListener,
        CompoundButton.OnCheckedChangeListener,
        TextWatcher {

    private static final String TAG = RequestActivity.class.getSimpleName();
    // rotation values for when a view is expanded or collapsed and duration of animation
    private static final float ROTATION_EXPANDED = 180f;
    private static final float ROTATION_COLLAPSED = 0f;
    private static final int ROTATION_ANIM_DURATION = 200;

    // layouts for holding the dynamic layout
    private LinearLayout mHeadersHolder, mBodyParamsHolder;
    private RelativeLayout mBodyParamsTypeLayout, mBodyParamsTitleLayout;
    private ScrollView mMainScrollView;
    private EditText mBodyRawET, mTimeoutValueET;
    private TextInputLayout mUrlTil;
    private Spinner mMethodSpinner;
    private Button mSendRequestBTN, mAddBodyBTN, mAddHeaderBTN;
    private ImageButton mExpandHeadersIB, mExpandBodyIB;
    private RadioButton mKeyValueBodyRadio, mRawBodyRadio;
    private ProgressBar mSendRequestPB;
    private Menu mMenu;
    private List<LinearLayoutSerialized> mHeaderLayouts = new ArrayList<>();
    private List<LinearLayoutSerialized> mBodyParamLayouts = new ArrayList<>();
    private ResponseDialogFragment mResponseDialog = null;
    private SendRequestTask mSendRequestTask;
    private RequestData mPassedRequestData;
    // needed so the save toggle isn't called when spinner is initialized
    private int mInitializeSpinnerCheck = 0;

    /**
     * Save values of views and lists on orientation changes, otherwise vales get reset to empty or
     * if viewing a saved item values will be reset to the original saved items values.
     *
     * @param outState - bundle to store the data
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.SAVED_STATE_REQUEST_URL, mUrlTil.getEditText().getText().toString());
        outState.putString(Constants.SAVED_STATE_REQUEST_METHOD, mMethodSpinner.getSelectedItem().toString());
        outState.putString(Constants.SAVED_STATE_REQUEST_TIMEOUT, mTimeoutValueET.getText().toString());
        outState.putSerializable(Constants.SAVED_STATE_REQUEST_HEADER_LAYOUTS, (ArrayList) mHeaderLayouts);
        outState.putString(Constants.SAVED_STATE_BODY_REQUEST_RAW, mBodyRawET.getText().toString());
        outState.putSerializable(Constants.SAVED_STATE_REQUEST_HEADER_KEY_VALUES,
                new LinkedHashMap<>(Utils.buildMapFromLayout(mHeaderLayouts)));
        outState.putSerializable(Constants.SAVED_STATE_REQUEST_BODY_LAYOUTS, (ArrayList) mBodyParamLayouts);
        outState.putSerializable(Constants.SAVED_STATE_REQUEST_BODY_KEY_VALUES,
                new LinkedHashMap<>(Utils.buildMapFromLayout(mBodyParamLayouts)));
        // check if the views were expanded based on the expanded value
        outState.putBoolean(Constants.SAVED_STATE_IS_BODY_EXPANDED, mExpandBodyIB.getRotation() == ROTATION_EXPANDED);
        outState.putBoolean(Constants.SAVED_STATE_IS_HEADERS_EXPANDED, mExpandHeadersIB.getRotation() == ROTATION_EXPANDED);
        outState.putBoolean(Constants.SAVED_STATE_BODY_RAW_IS_CHECKED, mRawBodyRadio.isChecked());
        if (mResponseDialog != null) {
            outState.putParcelable(Constants.SAVED_STATE_RESPONSE_DATA, mResponseDialog.getResponseData());
            outState.putBoolean(Constants.SAVED_STATE_RESPONSE_AVAIL, true);
        } else {
            outState.putBoolean(Constants.SAVED_STATE_RESPONSE_AVAIL, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_request, menu);
        mMenu = menu;

        // show the save icon on startup ONLY if it is a history item
        // saved items shouldn't be saved if they are identical which they will
        // be on initial launch. (invisible by default in xml)
        if (mPassedRequestData != null && !mPassedRequestData.isSaved()) {
            menu.findItem(R.id.action_save).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // depending on item id...
        switch (item.getItemId()) {
            case R.id.action_save:
                // update saved date to now
                mPassedRequestData.setCreatedAt(new Date());
                // if its a saved item update in Models
                if (mPassedRequestData.isSaved()) {
                    final long id = mPassedRequestData.getId();
                    // build request on new data in UI and reset id and new date
                    mPassedRequestData = buildRequest();
                    mPassedRequestData.setId(id);
                    // update item in DB
                    if (RequestModel.getInstance().updateSavedRequest(mPassedRequestData, this)) {
                        Toast.makeText(this, R.string.toast_msg_item_updated, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, R.string.toast_msg_item_update_failed, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // not a saved item so now set it as saved and add it to the saved item list
                    mPassedRequestData.setSaved(true);
                    RequestModel.getInstance().addSavedRequest(mPassedRequestData, this);
                    Toast.makeText(this, R.string.toast_msg_item_saved, Toast.LENGTH_SHORT).show();
                }
                // hide save icon
                item.setVisible(false);
                return true;
            case android.R.id.home:
                // finish activity on back arrow click
                finish();
                return true;
            case R.id.action_show_response:
                if (mResponseDialog != null) {
                    // show the response dialog
                    mResponseDialog.show(getSupportFragmentManager(), TAG);
                }
                return true;
        }
        return false;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        // this sets the AB title in recents to white and the AB darker
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            ActivityManager.TaskDescription taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), bm,
                    ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setTaskDescription(taskDesc);
        }

        // get extras passed with previous activity(MainViewPagerActivity)
        Bundle args = getIntent().getExtras();
        if (args != null) {
            // the RequestData class implements parcelable and was passed with getArguments' bundle
            mPassedRequestData = args.getParcelable(Constants.INTENT_EXTRA_PASSED_REQUEST);
            //Log.i(TAG, "Passed request is saved item: "+mPassedRequestData.isSaved());
        }

        // set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_request);
        setSupportActionBar(toolbar);
        // display back arrow in actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String abTitle;
        /*
         * Used for setting the actionbar title, depending on saved request, history request or
         * new request etc...
         * if passed request is not null its a saved or history request
         */
        if (mPassedRequestData != null) {
            if (mPassedRequestData.isSaved()) {
                abTitle = getString(R.string.fragment_title_request_saved);
            } else {
                abTitle = getString(R.string.fragment_title_request_history);
            }
        } else {
            // otherwise it is a new request
            abTitle = getString(R.string.fragment_title_request_new);
        }
        // set AB title
        getSupportActionBar().setTitle(abTitle);

        // load views and listeners
        bindActivity(savedInstanceState);

        // if orientation change set values from saved instance
        if (savedInstanceState != null) {
            setSavedInstanceValues(savedInstanceState);
        } else if (mPassedRequestData != null) {
            // otherwise value is a passed request so initialize values
            setPassedRequestValues();
        } else {
            // its a new request so create empty header row
            addNewHeaderRow();
        }
    }

    // method to load views and listeners, put into a method so onCreate isn't so big
    private void bindActivity(Bundle savedInstanceState) {
        mMainScrollView = (ScrollView) findViewById(R.id.scrollview_request);
        mUrlTil = (TextInputLayout) findViewById(R.id.til_url);
        mMethodSpinner = (Spinner) findViewById(R.id.spinner_request_method);
        mHeadersHolder = (LinearLayout) findViewById(R.id.headers_holder);
        mBodyParamsHolder = (LinearLayout) findViewById(R.id.body_params_holder);
        mBodyRawET = (EditText) findViewById(R.id.et_body_raw);
        mTimeoutValueET = (EditText) findViewById(R.id.et_timeout_value);
        mBodyParamsTypeLayout = (RelativeLayout) findViewById(R.id.body_layout);
        mBodyParamsTitleLayout = (RelativeLayout) findViewById(R.id.rl_body_title);
        mSendRequestPB = (ProgressBar) findViewById(R.id.pb_send_request);
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        mRawBodyRadio = (RadioButton) findViewById(R.id.radio_raw);
        mKeyValueBodyRadio = (RadioButton) findViewById(R.id.radio_keyval);
        mAddHeaderBTN = (Button) findViewById(R.id.btn_add_header_row);
        mAddBodyBTN = (Button) findViewById(R.id.btn_add_body_row);
        mSendRequestBTN = (Button) findViewById(R.id.btn_send_request);

        mExpandHeadersIB = (ImageButton) findViewById(R.id.ib_expand_request_headers);
        mExpandHeadersIB.setOnClickListener(this);

        mExpandBodyIB = (ImageButton) findViewById(R.id.ib_expand_request_body_params);
        mExpandBodyIB.setOnClickListener(this);

        mRawBodyRadio.setOnCheckedChangeListener(this);
        mKeyValueBodyRadio.setOnCheckedChangeListener(this);
        mSendRequestBTN.setOnClickListener(this);
        mAddHeaderBTN.setOnClickListener(this);
        mAddBodyBTN.setOnClickListener(this);
        // initialize adapters and spinner item click listener
        initializeAdapters(savedInstanceState, radioGroup);
    }

    private void initializeAdapters(final Bundle savedInstanceState, final RadioGroup radioGroup) {
        // Create a adapter for spinner items, array of items stored in arrays.xml
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.request_methods, R.layout.simple_spinner_item);
        // set custom drop down list
        spinnerAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        mMethodSpinner.setAdapter(spinnerAdapter);

        // listener for when spinner item is selected
        mMethodSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // if POST or PUT method then show body params layouts
                if (mMethodSpinner.getSelectedItem().equals(Constants.REQUEST_METHOD_POST)
                        || mMethodSpinner.getSelectedItem().equals(Constants.REQUEST_METHOD_PUT)) {
                    setParamLayoutVisibility(radioGroup, View.VISIBLE);
                } else {
                    // otherwise hide body params if it is visible (previous selection could be PUT or POST)
                    setParamLayoutVisibility(radioGroup, View.GONE);
                }
                /*
                 * spinner has a issue in that this listener gets called once the activity is started
                 * so use an initialization check to ensure this section of the code doesn;t get run.
                 */
                mInitializeSpinnerCheck = mInitializeSpinnerCheck + 1;
                if (mPassedRequestData != null) {
                    // don't run this the first time around on initialization
                    if (mInitializeSpinnerCheck > 1) {
                        // if savedInstanceState is not null and it contains the METHOD key
                        if (savedInstanceState != null) {
                            // check that the value matched that of the currently selected item in the spinner
                            // or the PassedRequestData's method is equal to that of the currently spinner selected item.
                            if (!savedInstanceState.getString(Constants.SAVED_STATE_REQUEST_METHOD)
                                    .equals(mPassedRequestData.getMethod())
                                    || !mPassedRequestData.getMethod().equals(mMethodSpinner.getSelectedItem().toString())) {
                                // check if mPassRequestData is equal to the values in the UI
                                toggleSaveIcon();

                            }
                        } else {
                            // saved instance is null
                            toggleSaveIcon();
                        }
                    } else {
                        // this else is run on first run
                        Log.i(TAG, "Spinner not initialised!");
                        if (mPassedRequestData != null) {
                            //Log.d(TAG, "PassedRequest not NULL");
                            // add a Runnable with the post() method which will be executed after view is loaded.
                            // This must done or values will not be set so save menu icon will always be hidden.
                            mMainScrollView.post(new Runnable() {
                                @Override
                                public void run() {
                                    // rootView has loaded
                                    // item is a saved item so check if the values match the current values in UI
                                    if (mPassedRequestData.isSaved()) {
                                        toggleSaveIcon();
                                    } else {
                                        // its a history item so show the save icon in toolbar
                                        if (mMenu != null && mMenu.findItem(R.id.action_save) != null) {
                                            mMenu.findItem(R.id.action_save).setVisible(true);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        // the adapter for the autocomplete TextView URL - items in adapter are URLS entered in DB
        ArrayAdapter<String> autoCompleteAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, DBUtils.getAllRequestUrls(this));
        ((AutoCompleteTextView) mUrlTil.getEditText()).setAdapter(autoCompleteAdapter);

        // used to see whether or not to show save icon
        mUrlTil.getEditText().addTextChangedListener(this);
        // used to see whether or not to show save icon
        mTimeoutValueET.addTextChangedListener(this);
    }

    // used for changing the body layouts visibility
    private void setParamLayoutVisibility(RadioGroup radioGroup, int visibility) {
        mBodyParamsTitleLayout.setVisibility(visibility);
        radioGroup.setVisibility(visibility);
        mBodyParamsHolder.setVisibility(visibility);
    }

    // show toggle save icon in actionbar, do the appropriate checks to make sure no NPE
    private void toggleSaveIcon() {
        if (mMenu != null) {
            final MenuItem saveMenuItem = mMenu.findItem(R.id.action_save);
            if (saveMenuItem != null) {
                // if the request is the same as the current one in UI and the save icon is visible, hide it
                if (mPassedRequestData.isEqualTo(buildRequest())) {
                    Log.d(TAG, "Request Is Equal to current");
                    saveMenuItem.setVisible(false);
                } else {
                    saveMenuItem.setVisible(true);
                    Log.d(TAG, "Request Is NOT Equal to current");
                }
            }
        }
    }

    /**
     * Set the UI values when saved instance state is not NULL (on orientation change)
     *
     * @param savedInstanceState - bundle passed from onCreate or onCreateView
     */
    @SuppressWarnings("unchecked")
    private void setSavedInstanceValues(final Bundle savedInstanceState) {
        if (savedInstanceState.containsKey(Constants.SAVED_STATE_REQUEST_URL)) {
            mUrlTil.getEditText().setText(savedInstanceState.getString(Constants.SAVED_STATE_REQUEST_URL));
            mUrlTil.getEditText().setSelection(mUrlTil.getEditText().getText().length());
        }
        if (savedInstanceState.containsKey(Constants.SAVED_STATE_REQUEST_TIMEOUT)) {
            mTimeoutValueET.setText(savedInstanceState.getString(Constants.SAVED_STATE_REQUEST_TIMEOUT));
        }
        // BODY
        // get the body key/value pair from the LinkedHashMap
        LinkedHashMap<String, String> keyValBody = (LinkedHashMap) savedInstanceState
                .getSerializable(Constants.SAVED_STATE_REQUEST_BODY_KEY_VALUES);
        if (!keyValBody.isEmpty()) {
            // get the list of layouts from the bundle
            List<LinearLayoutSerialized> layoutsBody = (List) savedInstanceState
                    .getSerializable(Constants.SAVED_STATE_REQUEST_BODY_LAYOUTS);
            // add the listener to the layouts along with the values
            rebuildKeyValueLayout(keyValBody, layoutsBody, false);
            // if body params layouts aren't empty show the layout containing the radiobuttons and fab
            if (!mBodyParamLayouts.isEmpty()) {
                mExpandBodyIB.setRotation(ROTATION_EXPANDED);
                mBodyParamsTypeLayout.setVisibility(View.VISIBLE);
                mBodyParamsHolder.setVisibility(View.VISIBLE);
                mAddBodyBTN.setVisibility(View.VISIBLE);
            }
        } else if (!mRawBodyRadio.isChecked()) {
            // if the raw radio button is not checked and the list is empty then
            // show the default row for orientation change
            mBodyParamsHolder.setVisibility(View.VISIBLE);
            for (LinearLayout layout : mBodyParamLayouts) {
                layout.setVisibility(View.VISIBLE);
            }
            // clear body text as we can only use one or the other
            mBodyRawET.setText("");
            mAddBodyBTN.setVisibility(View.VISIBLE);
            // add new empty row
            addNewBodyRow();
        }
        // headers
        // get the body key/value pair from the LinkedHashMap
        LinkedHashMap<String, String> keyValHeaders = (LinkedHashMap<String, String>) savedInstanceState
                .getSerializable(Constants.SAVED_STATE_REQUEST_HEADER_KEY_VALUES);
        if (!keyValHeaders.isEmpty()) {
            List<LinearLayoutSerialized> layoutsHeaders = (List) savedInstanceState
                    .getSerializable(Constants.SAVED_STATE_REQUEST_HEADER_LAYOUTS);
            // add the listener to the layouts along with the values
            rebuildKeyValueLayout(keyValHeaders, layoutsHeaders, true);
        } else {
            //is empty so add blank row for headers
            addNewHeaderRow();
        }
        toggleBodyLayoutVisibility(savedInstanceState.getBoolean(Constants.SAVED_STATE_IS_BODY_EXPANDED));
        toggleHeaderLayoutVisibility(savedInstanceState.getBoolean(Constants.SAVED_STATE_IS_HEADERS_EXPANDED));
        // only need to check if one of them is checked because its a radio group if one is checked the other isn;t
        if (savedInstanceState.getBoolean(Constants.SAVED_STATE_BODY_RAW_IS_CHECKED)) {
            mRawBodyRadio.setChecked(true);
        } else {
            mKeyValueBodyRadio.setChecked(true);
        }
        // see if menu item to show response should be visisble
        if (savedInstanceState.getBoolean(Constants.SAVED_STATE_RESPONSE_AVAIL)) {
            // only load menu item visibility after last view is loaded
            mSendRequestBTN.post(new Runnable() {
                @Override
                public void run() {
                    showResponseMenuItem();
                }
            });
            if (savedInstanceState.containsKey(Constants.SAVED_STATE_RESPONSE_DATA)) {
                // ResponseData is also parcelable allowing it to be passed from this activity to the dialog fragment
                // could also be done using Model
                //Log.i(TAG, "Recreated response dialog with data: "+ savedInstanceState.getParcelable(Constants.SAVED_STATE_RESPONSE_DATA).toString());
                mResponseDialog = ResponseDialogFragment.newInstance(
                        (ResponseData) savedInstanceState.getParcelable(Constants.SAVED_STATE_RESPONSE_DATA));
            }
        }
    }

    /**
     * After orientation change both the headers layouts and body layouts need to be rebuilt if they exist
     * both use similar ways so extract the code to somewhere where can be called by both.
     *
     * @param keyValPair - key/value pair (text within layouts)
     * @param layouts    - the layouts themselves
     * @param isHeaders  - whether or not they are headers, if false they they are body key/value
     */
    private void rebuildKeyValueLayout(LinkedHashMap<String, String> keyValPair, List<LinearLayoutSerialized> layouts, boolean isHeaders) {
        // get the keys as a List
        List<String> keyList = new ArrayList<>(keyValPair.keySet());
        for (int i = 0; i < layouts.size(); i++) {
            // keyList can be more then the layoutsBody size if a layout has empty values
            // so check to avoid IndexOutOfBoundsException
            if (i < keyList.size()) {
                String key = keyList.get(i);
                String value = keyValPair.get(key);
                TextInputLayout keyTil = (TextInputLayout) layouts.get(i).findViewById(R.id.til_key);
                TextInputLayout valueTil = (TextInputLayout) layouts.get(i).findViewById(R.id.til_value);
                keyTil.getEditText().setText(key);
                valueTil.getEditText().setText(value);
            }
        }

        // rebuild the layouts
        for (LinearLayoutSerialized layout : layouts) {
            if (layout.getParent() != null) {
                // view must be removed before re-added or exception occurs
                ((ViewGroup) layout.getParent()).removeView(layout); // <- fix
            }
            if (!isHeaders) {
                mBodyParamLayouts.add(layout);
                mBodyParamsHolder.addView(layout);
                // reset the listeners on the views
                addListenersToKeyValueRow(layout, mBodyParamsHolder, false);
            } else {
                mHeaderLayouts.add(layout);
                mHeadersHolder.addView(layout);
                // reset the listeners on the views
                addListenersToKeyValueRow(layout, mHeadersHolder, true);
            }
        }
    }

    /**
     * Toggles the visibility of the body key/value layouts
     *
     * @param visible - is visible or not
     */
    private void toggleBodyLayoutVisibility(boolean visible) {
        if (visible) {
            mExpandBodyIB.animate().rotation(ROTATION_EXPANDED).setDuration(ROTATION_ANIM_DURATION);
            mBodyParamsTypeLayout.setVisibility(View.VISIBLE);
            // wait until last view is loaded before setting visibility, otherwise its we
            // get the wrong result eg no add row button or radio visibility not set properly
            mSendRequestBTN.post(new Runnable() {
                @Override
                public void run() {
                    if (mKeyValueBodyRadio.isChecked()) {
                        mBodyParamsHolder.setVisibility(View.VISIBLE);
                        // loop through each layout setting it visible if key/value radio is checked
                        for (LinearLayout layout : mBodyParamLayouts) {
                            layout.setVisibility(View.VISIBLE);
                        }
                        // if body layouts is empty then create a blank row
                        if (mBodyParamLayouts.isEmpty()) {
                            addNewBodyRow();
                        }
                        mAddBodyBTN.setVisibility(View.VISIBLE);
                    } else {
                        // raw radio must be checked so set raw ETs visibility back to visisble
                        mBodyRawET.setVisibility(View.VISIBLE);
                    }
                }
            });

            //mBodyRawET.setVisibility(mBodyParamLayouts.size() > 0 ? View.GONE : View.VISIBLE);
        } else {
            mExpandBodyIB.animate().rotation(ROTATION_COLLAPSED).setDuration(ROTATION_ANIM_DURATION);
            mBodyParamsTypeLayout.setVisibility(View.GONE);
            mBodyParamsHolder.setVisibility(View.GONE);
            for (LinearLayout layout : mBodyParamLayouts) {
                layout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Toggles the visibility of the header key/value rows.
     *
     * @param visible - is visible or not
     */
    private void toggleHeaderLayoutVisibility(boolean visible) {
        if (visible) {
            mExpandHeadersIB.animate().rotation(ROTATION_EXPANDED).setDuration(ROTATION_ANIM_DURATION);
            mAddHeaderBTN.setVisibility(View.VISIBLE);
            mHeadersHolder.setVisibility(View.VISIBLE);
            // show each row if there are any
            for (LinearLayout layout : mHeaderLayouts) {
                layout.setVisibility(View.VISIBLE);
            }
        } else {
            mExpandHeadersIB.animate().rotation(ROTATION_COLLAPSED).setDuration(ROTATION_ANIM_DURATION);
            mAddHeaderBTN.setVisibility(View.GONE);
            mHeadersHolder.setVisibility(View.GONE);
            // hide each row if there are any
            for (LinearLayout layout : mHeaderLayouts) {
                layout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * Sets the UI values to that of the passed RequestData from getArguments()'s bundle
     */
    private void setPassedRequestValues() {
        mUrlTil.getEditText().setText(mPassedRequestData.getUrl());
        mUrlTil.getEditText().setSelection(mPassedRequestData.getUrl().length());
        mTimeoutValueET.setText(String.valueOf(mPassedRequestData.getTimeOut()));
        mMethodSpinner.setSelection(Arrays.asList(getResources().getStringArray(R.array.request_methods)).indexOf(mPassedRequestData.getMethod()));
        if (mPassedRequestData.getBodyKeyValuePair() != null
                && !mPassedRequestData.getBodyKeyValuePair().isEmpty()) {
            // its important that a LinkedHashMap is used here to keep the insertion order of these kay/values
            for (LinkedHashMap.Entry<String, String> entry : mPassedRequestData.getBodyKeyValuePair().entrySet()) {
                LinearLayoutSerialized layout = createKeyValueRow(mBodyParamsHolder, false);
                TextInputLayout keyTil = (TextInputLayout) layout.findViewById(R.id.til_key);
                TextInputLayout valueTil = (TextInputLayout) layout.findViewById(R.id.til_value);
                keyTil.getEditText().setText(entry.getKey());
                valueTil.getEditText().setText(entry.getValue());
                mBodyParamLayouts.add(layout);
                mBodyParamsHolder.addView(layout);
            }
            mKeyValueBodyRadio.setChecked(true);
            toggleBodyLayoutVisibility(true);
        } else if (!TextUtils.isEmpty(mPassedRequestData.getBodyRaw())) {
            // if body raw is not empty set the radio for raw checked
            mRawBodyRadio.setChecked(true);
            mBodyRawET.setText(mPassedRequestData.getBodyRaw());
            toggleBodyLayoutVisibility(true);
            mAddBodyBTN.setVisibility(View.GONE);
        }

        // Request headers, setting its layouts and adding them to the holder layout
        if (mPassedRequestData.getHeaders() != null && !mPassedRequestData.getHeaders().isEmpty()) {
            // its important that a LinkedHashMap is used here to keep the insertion order of these kay/values
            for (LinkedHashMap.Entry<String, String> entry : mPassedRequestData.getHeaders().entrySet()) {
                LinearLayoutSerialized layout = createKeyValueRow(mHeadersHolder, true);
                TextInputLayout keyTil = (TextInputLayout) layout.findViewById(R.id.til_key);
                TextInputLayout valueTil = (TextInputLayout) layout.findViewById(R.id.til_value);
                keyTil.getEditText().setText(entry.getKey());
                valueTil.getEditText().setText(entry.getValue());
                mHeaderLayouts.add(layout);
                mHeadersHolder.addView(layout);
            }
            toggleHeaderLayoutVisibility(true);
        } else {
            // add header row if no headers exist, just so its available to the user when activity is launched
            addNewHeaderRow();
        }
    }

    /**
     * Build RequestData object based on the values currently in the UI
     *
     * @return RequestData
     */
    private RequestData buildRequest() {
        RequestData requestData = new RequestData();
        requestData.setUrl(mUrlTil.getEditText().getText().toString());
        requestData.setMethod(mMethodSpinner.getSelectedItem().toString());
        requestData.setBodyRaw(mBodyRawET.getText().toString());
        requestData.setBodyKeyValuePair(Utils.buildMapFromLayout(mBodyParamLayouts));
        requestData.setHeaders(Utils.buildMapFromLayout(mHeaderLayouts));
        requestData.setTimeOut(Integer.parseInt(mTimeoutValueET.getText().toString()));
        return requestData;
    }

    /**
     * Send the request and run it in the background using the {@link SendRequestTask}
     * Implement Request Listener which is nested with the {@link SendRequestTask}
     * {@link SendRequestTask.RequestCallback#onError(String)} needs to be run on UI thread as it is
     * sent from the work done in the background thread
     * on the AsyncTask same goes for {@link SendRequestTask.RequestCallback#onConnectionTimeout()}.
     */
    private void sendRequest() {
        final RequestData requestData = buildRequest();
        // if task is not null and is running, then cancel it
        if (mSendRequestTask != null
                && mSendRequestTask.getStatus() == AsyncTask.Status.RUNNING) {
            mSendRequestTask.cancel(true);
        }
        // execute new task with anonymous listener
        mSendRequestTask = new SendRequestTask(new SendRequestTask.RequestCallback() {
            @Override
            public void onStart() {
                mSendRequestPB.setVisibility(View.VISIBLE);
                mSendRequestBTN.setText(getString(R.string.btn_sending_request));
                // disable button while running the request
                mSendRequestBTN.setEnabled(false);
            }

            @Override
            public void onConnectionTimeout() {
                runResultOnUIThread(getString(R.string.toast_msg_connection_timeout));
            }

            @Override
            public void onError(final String error) {
                runResultOnUIThread(getString(R.string.toast_msg_error, error));
            }

            @Override
            public void onComplete(ResponseData responseData) {
                // reset the button text and enable plus hide the PB bar
                mSendRequestBTN.setText(getString(R.string.btn_send_request));
                mSendRequestPB.setVisibility(View.GONE);
                mSendRequestBTN.setEnabled(true);
                // add the new item to the history list at the first position, most recent...
                RequestModel.getInstance().addHistoryRequest(0, requestData, RequestActivity.this);
                if (responseData != null) {
                    // if the response data is not null show the response dialog
                    mResponseDialog = ResponseDialogFragment.newInstance(responseData);
                    mResponseDialog.show(getSupportFragmentManager(), TAG);
                    // add a 200 milli-second delay to showing response icon in toolbar so it shows after the dialog
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showResponseMenuItem();
                        }
                    }, ROTATION_ANIM_DURATION);
                }
            }
        });
        // start the AsyncTask
        mSendRequestTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, requestData);
    }

    // show the response menu item in action bar if response is received
    private void showResponseMenuItem() {
        if (mMenu != null) {
            MenuItem showResponseItem = mMenu.findItem(R.id.action_show_response);
            if (showResponseItem != null) {
                showResponseItem.setVisible(true);
            }
        }
    }

    /**
     * Both RequestListeners onError and onConnectionTimeout both run in the background of
     * AsyncTask, the errors are reported from there so tun their result on the UI thread
     * as changes to UI can not be run on background threads but only Ui thread.
     *
     * @param result - message to show in toast
     * @see SendRequestTask.RequestCallback
     */
    private void runResultOnUIThread(final String result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSendRequestBTN.setText(getString(R.string.btn_send_request));
                mSendRequestPB.setVisibility(View.GONE);
                mSendRequestBTN.setEnabled(true);
                Toast.makeText(RequestActivity.this, result, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Create a key value row for either headers or body params.
     * This calls {@link #addListenersToKeyValueRow(LinearLayoutSerialized, LinearLayout, boolean)}
     * which adds the listeners for buttons and text changed and toggles save icon.
     *
     * @param holderView view to add the layout to
     * @param isHeader   - is header or body param
     * @return - Serialized LinearLayout, serialized so it can be used in savedInstance
     */
    private LinearLayoutSerialized createKeyValueRow(final LinearLayout holderView, final boolean isHeader) {
        @SuppressLint("InflateParams") final LinearLayoutSerialized layout = (LinearLayoutSerialized)
                getLayoutInflater().inflate(R.layout.row_key_value, null);
        addListenersToKeyValueRow(layout, holderView, isHeader);
        return layout;
    }

    /**
     * Used for adding listeners to the dynamic layouts added for header and body
     * params key/value pair.
     *
     * @param layout     - layout to add listener to
     * @param holderView - Layout which is the parent container of layout #param
     * @param isHeader   - if it is headers or body params
     */
    private void addListenersToKeyValueRow(final LinearLayoutSerialized layout, final LinearLayout holderView, final boolean isHeader) {
        final FloatingActionButton deleteRow = (FloatingActionButton) layout.findViewById(R.id.fab_delete_row);
        deleteRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holderView.removeView(layout);
                if (isHeader) {
                    mHeaderLayouts.remove(layout);
                    toggleSaveIconForChange(false);
                } else {
                    mBodyParamLayouts.remove(layout);
                    toggleSaveIconForChange(true);
                }
            }
        });

        TextInputLayout tilKey = (TextInputLayout) layout.findViewById(R.id.til_key);
        tilKey.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // add a check for when to show save icon in headers or key body params are changed
                if (mPassedRequestData != null) {
                    if (mMenu != null) {
                        final MenuItem saveMenuItem = mMenu.findItem(R.id.action_save);
                        if (saveMenuItem != null) {
                            saveMenuItem.setVisible(
                                    isHeader ? !mPassedRequestData.getHeaders().containsKey(editable.toString())
                                            : !mPassedRequestData.getBodyKeyValuePair().containsKey(editable.toString()));
                        }
                    }
                }
                //Log.i(TAG, "Contains value: " + String.valueOf(mPassedRequestData.getBodyKeyValuePair().containsKey(editable.toString())));
            }
        });
        TextInputLayout tilValue = (TextInputLayout) layout.findViewById(R.id.til_value);
        tilValue.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // add a check for when to show save icon in headers or key body params are changed
                if (mPassedRequestData != null) {
                    if (mMenu != null) {
                        final MenuItem saveMenuItem = mMenu.findItem(R.id.action_save);
                        if (saveMenuItem != null) {
                            saveMenuItem.setVisible(
                                    isHeader ? !mPassedRequestData.getHeaders().containsValue(editable.toString())
                                            : !mPassedRequestData.getBodyKeyValuePair().containsValue(editable.toString()));
                        }
                    }
                }
            }
        });
    }

    /**
     * Toggles the save icon is the parameter size changes
     */
    private void toggleSaveIconForChange(boolean isBody) {
        if (mPassedRequestData != null) {
            if (mMenu != null && mMenu.findItem(R.id.action_save) != null) {
                // if menu and item are not null and item is NOT visible then make it so
                if (isBody) {
                    if (mPassedRequestData.getBodyKeyValuePair().size() != mBodyParamLayouts.size()) {
                        mMenu.findItem(R.id.action_save).setVisible(true);
                    } else {
                        mMenu.findItem(R.id.action_save).setVisible(false);
                    }
                } else {
                    if (mPassedRequestData.getHeaders().size() != mHeaderLayouts.size()) {
                        mMenu.findItem(R.id.action_save).setVisible(true);
                    } else {
                        mMenu.findItem(R.id.action_save).setVisible(false);
                    }
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_add_header_row:
                // create new row
                addNewHeaderRow();
                toggleSaveIconForChange(false);
                break;
            case R.id.btn_add_body_row:
                // create new row
                addNewBodyRow();
                toggleSaveIconForChange(true);
                break;
            case R.id.btn_send_request:
                // before sending request check that the URl is not empty
                if (Utils.isValidEntry(mUrlTil, getString(R.string.error_empty_url))) {
                    // check that there is a internet connection available
                    if (Utils.hasInternetConnection(this)) {
                        // scroll to bottom when button is pressed
                        mMainScrollView.smoothScrollTo(0, mMainScrollView.getBottom());
                        // send the request
                        sendRequest();
                    } else {
                        Toast.makeText(this, R.string.toast_msg_no_internet, Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.ib_expand_request_headers:
                // toggle request headers layout
                toggleHeaderLayoutVisibility(mAddHeaderBTN.getVisibility() == View.GONE);
                break;
            case R.id.ib_expand_request_body_params:
                // toggle body params layout visibility
                toggleBodyLayoutVisibility(mBodyParamsTypeLayout.getVisibility() == View.GONE);
                break;
        }
    }

    // creates new header row and adds it to the List and holder view
    private void addNewHeaderRow() {
        LinearLayoutSerialized layoutSerialized = createKeyValueRow(mHeadersHolder, true);
        mHeaderLayouts.add(layoutSerialized);
        mHeadersHolder.addView(layoutSerialized);
    }

    // creates new body key/value row and adds it to the List and holder view
    private void addNewBodyRow() {
        LinearLayoutSerialized layoutSerialized = createKeyValueRow(mBodyParamsHolder, false);
        mBodyParamLayouts.add(layoutSerialized);
        mBodyParamsHolder.addView(layoutSerialized);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (mMethodSpinner.getSelectedItem().equals(Constants.REQUEST_METHOD_POST)
                || mMethodSpinner.getSelectedItem().equals(Constants.REQUEST_METHOD_PUT)) {
            switch (compoundButton.getId()) {
                case R.id.radio_keyval:
                    if (isChecked) {
                        mBodyParamsHolder.setVisibility(View.VISIBLE);
                        for (LinearLayout layout : mBodyParamLayouts) {
                            layout.setVisibility(View.VISIBLE);
                        }
                        // clear body text as we can only use one or the other
                        mBodyRawET.setText("");
                        mAddBodyBTN.setVisibility(View.VISIBLE);
                        if (mBodyParamLayouts.isEmpty()) {
                            // add new empty row if no body params have been added
                            addNewBodyRow();
                        }
                    } else {
                        // hide body key/value params layouts
                        mBodyParamsHolder.setVisibility(View.GONE);
                        for (LinearLayout layout : mBodyParamLayouts) {
                            layout.setVisibility(View.GONE);
                        }
                        mAddBodyBTN.setVisibility(View.GONE);
                    }
                    break;
                case R.id.radio_raw:
                    if (isChecked) {
                        mBodyRawET.setVisibility(View.VISIBLE);
                        // remove previous key value pair layouts
                        mBodyParamsHolder.removeAllViews();
                        mBodyParamLayouts = new ArrayList<>();
                    } else {
                        mBodyRawET.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        // applied to some of the EditTexts or TILs
        if (mPassedRequestData != null) {
            toggleSaveIcon();
        }
    }

    @Override
    protected void onDestroy() {
        // cancel AsyncTask if running in onDestroy
        if (mSendRequestTask != null && mSendRequestTask.getStatus() == AsyncTask.Status.RUNNING) {
            mSendRequestTask.cancel(true);
        }
        super.onDestroy();
    }
}
