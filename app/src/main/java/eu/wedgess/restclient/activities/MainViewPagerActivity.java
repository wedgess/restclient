package eu.wedgess.restclient.activities;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.LinkedHashMap;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.adapters.ViewPagerAdapter;
import eu.wedgess.restclient.fragments.HistoryRequestFragment;
import eu.wedgess.restclient.fragments.SavedRequestFragment;

/**
 * Created by Gareth on 11/11/2016.
 **/
public class MainViewPagerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view_pager);

        // this sets the ActionBar title in the recents screen to white and the ActionBar darker
        // only works on Lollipop and Above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            ActivityManager.TaskDescription taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), bm,
                    ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setTaskDescription(taskDesc);
        }

        // set up toolbar and set it as actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // get a reference to the ViewPager from the layout file
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), getTabbedFragments());
        // set-up ViewPager with adapter
        mViewPager.setAdapter(adapter);
        // don't refresh fragments when swiped, set offscreen limit as two
        mViewPager.setOffscreenPageLimit(2);

        // set up tabs with viewpager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    /*
    * add the items to the LinkedHashMap which gets used as the tabs in the ViewPager
    */
    private LinkedHashMap<String, Fragment> getTabbedFragments() {
        // keep order of the items so use a LinkedHashMap
        LinkedHashMap<String, Fragment> tabbedItems = new LinkedHashMap<>();
        tabbedItems.put(getString(R.string.tab_title_history), new HistoryRequestFragment());
        tabbedItems.put(getString(R.string.tab_title_saved), new SavedRequestFragment());
        return tabbedItems;
    }
}
