package eu.wedgess.restclient.model;

import android.content.Context;
import android.util.Log;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import eu.wedgess.restclient.db.DBUtils;

/**
 * Main model class - This class is a singleton so that the same instance can be accessed from within
 * any of the classes.
 * <p>
 * Created by Gareth on 10/11/2016.
 */

public class RequestModel {

    private static final String TAG = RequestModel.class.getSimpleName();
    // Used by HistoryRequestFragment
    private List<RequestData> mHistoryRequestDataList;
    // used by SavedRequestFragment
    private List<RequestData> mSavedRequestDataList;
    private static RequestModel mInstance;

    private RequestModel() {
        // non-instantiable
    }

    // returns an instance of this class, only way to access it
    public static RequestModel getInstance() {
        if (mInstance == null) {
            mInstance = new RequestModel();
        }
        return mInstance;
    }

    // initialize the request data list
    public void initializeData(Context context) {
        this.mHistoryRequestDataList = DBUtils.getAllRequests(false, context);
        this.mSavedRequestDataList = DBUtils.getAllRequests(true, context);
    }

    public List<RequestData> getHistoryRequestDataList() {
        return this.mHistoryRequestDataList;
    }

    public List<RequestData> getSavedRequestDataList() {
        return this.mSavedRequestDataList;
    }

    /**
     * Adds a history item to the DB and the History DataList.
     * History items go from most recent to least. Therefore new items get added
     * at position 0.
     *
     * @param position    - position to add item
     * @param requestData - item to add
     * @param context     - current context
     */
    public void addHistoryRequest(int position, RequestData requestData, Context context) {
        Log.i(TAG, "Adding requestData");
        // set current date so it is not null in the list, only items pulled from DB have date set
        requestData.setCreatedAt(new Date());
        long id = DBUtils.insertRequestRow(requestData, context);
        // set the id from the DB insertion
        requestData.setId(id);
        this.mHistoryRequestDataList.add(position, requestData);
    }

    /**
     * Adds a saved item to the DB and the SavedDataList
     *
     * @param requestData - item to add
     * @param context     - current context
     */
    public void addSavedRequest(RequestData requestData, Context context) {
        Log.i(TAG, "Adding requestData");
        // make sure RequestData object is set as saved
        requestData.setSaved(true);
        long id = DBUtils.insertRequestRow(requestData, context);
        requestData.setId(id);
        // add to start of list because it is today
        this.mSavedRequestDataList.add(0, requestData);
    }

    /**
     * Update a saved request item in saved list and DB
     *
     * @param requestData - RequestData to update
     * @param context     - current context
     * @return - whether or not item was updated
     */
    public boolean updateSavedRequest(RequestData requestData, Context context) {
        int position = getPositionInSavedList(requestData);
        if (position != -1) {
            this.mSavedRequestDataList.remove(position);
            this.mSavedRequestDataList.add(position, requestData);
            DBUtils.updateRequestRow(requestData, context);
            return true;
        }
        return false;
    }

    /**
     * Gets the position of a RequestData in the Saved Items List
     *
     * @param requestData - to find position of
     * @return position:int
     */
    private int getPositionInSavedList(RequestData requestData) {
        int index = -1;
        for (int i = 0; i < mSavedRequestDataList.size(); i++) {
            Log.i(TAG, "Saved Request List Item ID: " + mSavedRequestDataList.get(i).getId());
            Log.i(TAG, "Passed Request Item ID: " + requestData.getId());
            if (mSavedRequestDataList.get(i).getId() == requestData.getId()) {
                index = i;
                break;
            }
        }
        return index;
    }


    /**
     * Creates dummy data to show the application working, this gets created when user first starts
     * application or clears data. This is only for testing purposes.
     * TODO: Testing purposes only, could have used SharedPreferences here but then items
     * TODO: would only be added once so better to add this to test multiple entries etc..
     *
     * @param context
     */
    public void seedDBData(Context context) {
        // test site
        addHistoryRequest(0, new RequestData("https://jsonplaceholder.typicode.com/posts/1", "PATCH", "",
                15000, null, null, new Date()), context);
        // lotto results for the last irish, euromillions, daily millions etc
        addHistoryRequest(0, new RequestData("http://resultsservice.lottery.ie/resultsservice.asmx/GetResults", "POST",
                "drawType=All&lastNumberOfDraws=1", 15000, null, null, new Date()), context);
        // dublin bus info for some stop, get format as JSON
        addHistoryRequest(0, new RequestData("https://data.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?stopid=7602&format=json", "GET", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
        // another few tests
        addHistoryRequest(0, new RequestData("https://jsonplaceholder.typicode.com/todos", "OPTIONS", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
        addHistoryRequest(0, new RequestData("https://jsonplaceholder.typicode.com/todos", "DELETE", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
        addHistoryRequest(0, new RequestData("https://jsonplaceholder.typicode.com/todos", "PUT", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
        // test site
        addHistoryRequest(0, new RequestData("https://jsonplaceholder.typicode.com/posts/1", "GET", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
        // get dart information for glenageary
        addHistoryRequest(0, new RequestData("http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByNameXML?StationDesc=Glenageary", "GET", "",
                15000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date()), context);
    }

}
