package eu.wedgess.restclient.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * The main model class which holds the requests data.
 * This is the object which is stored in the DB. It's parcelable so that it can be stored in
 * save instance state by the adapter.
 * <p>
 * Created by Gareth on 10/11/2016.
 */

public class RequestData implements Parcelable {

    private long id;
    private String url;
    private String method;
    private String bodyRaw;
    private int timeOut;
    // use a LinkedHashMap to keep key/value insertion order, important in the UI of RequestFragment
    private LinkedHashMap<String, String> bodyKeyValuePair;
    private LinkedHashMap<String, String> headers;
    private Date createdAt;
    private boolean saved;

    public RequestData(long id, String url, String method, String bodyRaw, int timeout, LinkedHashMap<String, String> bodyKeyValuePair,
                       LinkedHashMap<String, String> headers, Date createdAt) {
        this.id = id;
        this.url = url;
        this.method = method;
        this.bodyRaw = bodyRaw;
        this.timeOut = timeout;
        this.bodyKeyValuePair = bodyKeyValuePair;
        this.headers = headers;
        this.createdAt = createdAt;
    }

    public RequestData(String url, String method, String bodyRaw, int timeout, LinkedHashMap<String, String> bodyKeyValuePair,
                       LinkedHashMap<String, String> headers, Date createdAt) {
        this(-1, url, method, bodyRaw, timeout, bodyKeyValuePair, headers, createdAt);
    }

    public RequestData(String url, String method, String bodyRaw, int timeout, LinkedHashMap<String, String> bodyKeyValuePair,
                       LinkedHashMap<String, String> headers) {
        this(-1, url, method, bodyRaw, timeout, bodyKeyValuePair, headers, new Date());
    }

    public RequestData() {
        // initialize as empty, cause we check is equal to below, if we leave values null we get a NPE when checking isEqualTo
        this(-1, "", "", "", 12000, new LinkedHashMap<String, String>(), new LinkedHashMap<String, String>(), new Date());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String uri) {
        this.url = uri;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getBodyRaw() {
        return bodyRaw;
    }

    public void setBodyRaw(String bodyRaw) {
        this.bodyRaw = bodyRaw;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    public LinkedHashMap<String, String> getBodyKeyValuePair() {
        return bodyKeyValuePair;
    }

    public void setBodyKeyValuePair(LinkedHashMap<String, String> bodyKeyValuePair) {
        this.bodyKeyValuePair = bodyKeyValuePair;
    }

    public LinkedHashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(LinkedHashMap<String, String> headers) {
        this.headers = headers;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean isSaved() {
        return saved;
    }

    /**
     * Ensure correct encoding of key value
     *
     * @return String of encoded params eg: key value changed to &key=value
     */
    @SuppressWarnings("StringConcatenationInsideStringBufferAppend")
    public String getBodyKeyValueEncodedParams() {
        StringBuilder sb = new StringBuilder();
        // for each key in the body LinkedHashMap
        for (String key : bodyKeyValuePair.keySet()) {
            String value = null;
            try {
                // get the value based on the key and encode it,
                value = URLEncoder.encode(bodyKeyValuePair.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // if this is the first value in SB append an &
            if (sb.length() > 0) {
                sb.append("&");
            }
            // make sure key and value are formatted as key=value
            sb.append(key + "=" + value);
        }
        return sb.toString();
    }

    /**
     * Checks to see if a RequestData visible in the UI is equal to this
     * RequestData object.
     *
     * @param data - RequestData to check if is equal to this one
     * @return - if equal or not
     */
    public boolean isEqualTo(RequestData data) {
        return this.getUrl().equals(data.getUrl())
                && this.getMethod().equals(data.getMethod())
                && this.getBodyRaw().equals(data.getBodyRaw())
                && this.getBodyKeyValuePair().equals(data.getBodyKeyValuePair())
                && this.getHeaders().equals(data.getHeaders())
                && this.getTimeOut() == data.getTimeOut();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.url);
        dest.writeString(this.method);
        dest.writeString(this.bodyRaw);
        dest.writeInt(this.timeOut);
        dest.writeSerializable(this.bodyKeyValuePair);
        dest.writeSerializable(this.headers);
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeByte(this.saved ? (byte) 1 : (byte) 0);
    }

    protected RequestData(Parcel in) {
        this.id = in.readLong();
        this.url = in.readString();
        this.method = in.readString();
        this.bodyRaw = in.readString();
        this.timeOut = in.readInt();
        this.bodyKeyValuePair = (LinkedHashMap<String, String>) in.readSerializable();
        this.headers = (LinkedHashMap<String, String>) in.readSerializable();
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        this.saved = in.readByte() != 0;
    }

    public static final Parcelable.Creator<RequestData> CREATOR = new Parcelable.Creator<RequestData>() {
        @Override
        public RequestData createFromParcel(Parcel source) {
            return new RequestData(source);
        }

        @Override
        public RequestData[] newArray(int size) {
            return new RequestData[size];
        }
    };
}
