package eu.wedgess.restclient.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for recording the data to be displayed back in the ResponseFragment.
 * <p>
 * Make the object immutable by using final as the response data will never change once set
 * <p>
 * Created by Gareth on 11/11/2016.
 */

public class ResponseData implements Parcelable {

    private final Map<String, String> headers;
    private final int statusCode;
    private final long timeTaken;
    private final String body;

    public ResponseData(Map<String, String> headers, int statusCode, long timeTaken, String body) {
        this.headers = headers;
        this.statusCode = statusCode;
        this.timeTaken = timeTaken;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.headers.size());
        for (Map.Entry<String, String> entry : this.headers.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
        dest.writeInt(this.statusCode);
        dest.writeLong(this.timeTaken);
        dest.writeString(this.body);
    }

    protected ResponseData(Parcel in) {
        int headersSize = in.readInt();
        this.headers = new HashMap<String, String>(headersSize);
        for (int i = 0; i < headersSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.headers.put(key, value);
        }
        this.statusCode = in.readInt();
        this.timeTaken = in.readLong();
        this.body = in.readString();
    }

    public static final Parcelable.Creator<ResponseData> CREATOR = new Parcelable.Creator<ResponseData>() {
        @Override
        public ResponseData createFromParcel(Parcel source) {
            return new ResponseData(source);
        }

        @Override
        public ResponseData[] newArray(int size) {
            return new ResponseData[size];
        }
    };
}
