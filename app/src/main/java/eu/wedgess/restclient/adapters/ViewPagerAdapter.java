package eu.wedgess.restclient.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.LinkedHashMap;

/**
 * This class is used by {@link eu.wedgess.restclient.activities.MainViewPagerActivity} as an
 * adapter for displaying mTabbedItems in the ViewPager.
 * <p>
 * Created by Gareth on 11/11/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    // List for holding the title and the the mTabbedItems fragment
    private LinkedHashMap<String, Fragment> mTabbedItems;

    public ViewPagerAdapter(FragmentManager manager, LinkedHashMap<String, Fragment> tabbedItems) {
        super(manager);
        this.mTabbedItems = tabbedItems;
    }

    @Override
    public Fragment getItem(int position) {
        // get the values of the LinkedHashMap as an array and get the position as a Fragment by casting
        return (Fragment) mTabbedItems.values().toArray()[position];
    }

    @Override
    public int getCount() {
        return mTabbedItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // get the keys of the LinkedHashMap as an array and get the position as a String (keys are Strings anyway)
        return mTabbedItems.keySet().toArray()[position].toString();
    }
}
