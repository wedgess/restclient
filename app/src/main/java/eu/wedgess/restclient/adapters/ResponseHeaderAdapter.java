package eu.wedgess.restclient.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Map;

import eu.wedgess.restclient.R;

/**
 * Adapter which displays the response headers in a RecyclerView.
 * No selection or anything needed this is simply just for viewing purposes.
 * <p>
 * Created by Gareth on 13/11/2016.
 */
public class ResponseHeaderAdapter extends RecyclerView.Adapter<ResponseHeaderAdapter.ResponseHeaderViewHolder> {

    // map of header data, order doesn't matter
    private Map<String, String> mHeaderData;

    public ResponseHeaderAdapter(Map<String, String> headerData) {
        this.mHeaderData = headerData;
    }

    @Override
    public ResponseHeaderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate the view used by ViewHolder
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_response_header, parent, false);
        return new ResponseHeaderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ResponseHeaderViewHolder holder, int position) {
        // this may not be the cleanest way to receive key and value by position
        // but better to store the data as a map than 2 Lists.
        // because we use a Map the header order will not be retained. But doesn't matter in this case.
        holder.headerKey.setText(mHeaderData.keySet().toArray()[position].toString());
        holder.headerValue.setText(mHeaderData.values().toArray()[position].toString());
    }

    @Override
    public int getItemCount() {
        return mHeaderData.size();
    }

    // used for storing the adapters data in onSavedInstanceState of ResponseDialogFragment
    public Map<String, String> getData() {
        return this.mHeaderData;
    }

    // use a static inner class to avoid memory leaks, explanation here: https://goo.gl/6WACMy
    static class ResponseHeaderViewHolder extends RecyclerView.ViewHolder {

        TextView headerKey;
        TextView headerValue;

        ResponseHeaderViewHolder(View itemView) {
            super(itemView);
            // get reference to the two TVs
            headerKey = (TextView) itemView.findViewById(R.id.tv_header_key);
            headerValue = (TextView) itemView.findViewById(R.id.tv_header_value);
        }
    }
}
