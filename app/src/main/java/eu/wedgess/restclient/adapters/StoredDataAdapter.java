package eu.wedgess.restclient.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.fragments.HistoryRequestFragment;
import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.utils.Constants;
import eu.wedgess.restclient.utils.RecyclerItemClickListener;
import eu.wedgess.restclient.utils.Utils;

/**
 * Serves as an adapter for RequestData items
 * This adapter is used by both {@link eu.wedgess.restclient.fragments.SavedRequestFragment}
 * and {@link HistoryRequestFragment}. It extends
 * {@link ExtendedAdapter} for handling adds and removed which extends {@link SelectableAdapter}
 * to facilitate Multi-select, multi-delete and undoing deleted items etc.
 * <p>
 * Created by Gareth on 10/11/2016.
 */
public class StoredDataAdapter extends ExtendedAdapter<StoredDataAdapter.SavedDataViewHolder> {

    private static final String TAG = StoredDataAdapter.class.getSimpleName();
    private Context mContext;
    private boolean mDateIsVisible;
    private RecyclerItemClickListener mRecyclerItemClickListener;

    public StoredDataAdapter(List<RequestData> requestDataList, boolean dateIsVisible, Context context,
                             RecyclerItemClickListener recyclerItemClickListener) {
        // pass the list of items to the super class ExtendedAdapter
        super(requestDataList);
        this.mContext = context;
        this.mDateIsVisible = dateIsVisible;
        this.mRecyclerItemClickListener = recyclerItemClickListener;
    }

    @Override
    public StoredDataAdapter.SavedDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // same layout gets used for both History and SavedRequest Fragments
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_stored_data, parent, false);
        return new SavedDataViewHolder(itemView, mDateIsVisible, mRecyclerItemClickListener);
    }

    @Override
    public void onBindViewHolder(SavedDataViewHolder holder, int position) {

        RequestData request = getItem(position);
        holder.urlTV.setText(request.getUrl());
        // if date is visible set the date text, relative to today
        if (mDateIsVisible) {
            holder.dateTV.setText(Utils.getRelativeDate(request.getCreatedAt()));
        }
        int colorId = 0;
        String method = null;
        // depending on the request method get the text color and method text
        switch (request.getMethod()) {
            case Constants.REQUEST_METHOD_GET:
                colorId = R.color.request_get;
                method = Constants.REQUEST_METHOD_GET;
                break;
            case Constants.REQUEST_METHOD_POST:
                colorId = R.color.request_post;
                method = Constants.REQUEST_METHOD_POST;
                break;
            case Constants.REQUEST_METHOD_DELETE:
                colorId = R.color.request_delete;
                method = Constants.REQUEST_METHOD_DELETE;
                break;
            case Constants.REQUEST_METHOD_PATCH:
                colorId = R.color.request_patch;
                method = Constants.REQUEST_METHOD_PATCH;
                break;
            case Constants.REQUEST_METHOD_PUT:
                colorId = R.color.request_put;
                method = Constants.REQUEST_METHOD_PUT;
                break;
            case Constants.REQUEST_METHOD_OPTIONS:
                colorId = R.color.request_options;
                method = Constants.REQUEST_METHOD_OPTIONS;
                break;
        }
        if (method != null) {
            // if not null set the color and text for method
            holder.methodTV.setText(method);
            holder.methodTV.setTextColor(ContextCompat.getColor(mContext, colorId));
        }

        // set background color, if item is selected the BG color needs to be dark,
        // if item is in normal state set the background attr of selectable to get
        // touch feedback on item click, setting a color will override ripple animation.
        if (isSelected(position)) {
            holder.itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_bg));
        } else {
            // this is the normal color however we are using the attribute "selectableItemBackground"
            // so that item click gives touch feedback
            holder.itemView.setBackgroundResource(Utils.getSelectedItemBgAttr(mContext));
        }
    }

    // use a static inner class to avoid memory leaks, explanation here: https://goo.gl/6WACMy
    static class SavedDataViewHolder extends RecyclerView.ViewHolder {

        TextView urlTV, methodTV, dateTV;

        SavedDataViewHolder(View view, boolean dateIsVisible, final RecyclerItemClickListener recyclerItemClickListener) {
            super(view);
            urlTV = (TextView) view.findViewById(R.id.tv_saved_url);
            methodTV = (TextView) view.findViewById(R.id.tv_saved_method);
            dateTV = (TextView) view.findViewById(R.id.tv_date_updated);
            LinearLayout.LayoutParams params;
            // if date is visible it is a saved request
            if (dateIsVisible) {
                // if this is the SavedRequestFragment the date will be visible
                dateTV.setVisibility(View.VISIBLE);
                // set the params so that the date is visible by setting layout weight as 0.63
                params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.63f);
            } else {
                // no date so the view can fill the rest of the layout by setting the weight as 0.8
                params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.8f);
            }
            // set the gravity to center vertical this some how gets reset & then set the layouts params
            params.gravity = Gravity.CENTER_VERTICAL;
            urlTV.setLayoutParams(params);

            // set click listener on the root view in order to send interfaces click listener to implementing Fragment
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (recyclerItemClickListener != null) {
                        recyclerItemClickListener.onItemClick(getAdapterPosition());
                    }
                }
            });

            // set long click listener on the root view in order to send interfaces long click listener to implementing Fragment
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (recyclerItemClickListener != null) {
                        recyclerItemClickListener.onItemLongClick(getAdapterPosition());
                    }
                    return true;
                }
            });
        }
    }

    /**
     * Save the adapters items on orientation change
     *
     * @param outState Current state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(TAG, new ArrayList(getAllItems()));
    }

    /**
     * Restore adapters items on orientation change.
     *
     * @param savedInstanceState Previous state
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setItems((ArrayList) savedInstanceState.getParcelableArrayList(TAG));
    }
}
