package eu.wedgess.restclient.adapters;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Uses generics to allow being extended.
 * <p>
 * Created by Gareth on 10/11/2016.
 * Idea based on: https://goo.gl/ICFpyu by: Davide Steduto
 */

public abstract class SelectableAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private static final String TAG = SelectableAdapter.class.getSimpleName();
    private static final String SAVED_INSTANCE_MODE = "mode";


    // Default selection mode
    public static final int MODE_SINGLE = 1;
    // Multi selection mode
    public static final int MODE_MULTI = 2;

    // list of selected items
    private ArrayList<Integer> selectedItems;
    private int mode;

    // instantiate empty list and set mode to single when constructed
    SelectableAdapter() {
        this.selectedItems = new ArrayList<>();
        this.mode = MODE_SINGLE;
    }

    /**
     * Set the mode of the selection, MODE_SINGLE is the default:
     * <ul>
     * MODE_SINGLE tells adapter to react to single tap on an item (previous selection is cleared automatically);
     * MODE_MULTI tells adapter to save the position to the list of the  selected items.
     * </ul>
     *
     * @param mode MODE_SINGLE or MODE_MULTI
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * The current selection mode of the Adapter.
     *
     * @return current mode
     * @see #MODE_SINGLE
     * @see #MODE_MULTI
     */
    public int getMode() {
        return mode;
    }

    /**
     * Indicates if the item, at the provided position, is selected.
     *
     * @param position Position of the item to check.
     * @return true if the item is selected, false otherwise.
     */
    boolean isSelected(int position) {
        return selectedItems.contains(position);
    }

    /**
     * Toggle the selection status of the item at a given position. If in multi-select
     * mode then the item is added to the selectedItems list., if mode is SINGLE then
     * the previous selection is cleared, if any. If multi-select then we notify the
     * adapters notifyItemChanged to redraw item which is where we change BG color
     * to the selected item color.
     *
     * @param position Position of the item to toggle selection
     */
    public void toggleSelection(int position) {
        if (position < 0) {
            return;
        }
        if (mode == MODE_SINGLE) {
            clearSelection();
        }

        int index = selectedItems.indexOf(position);
        if (index != -1) {
            Log.v(TAG, "toggleSelection removing selection on position " + position);
            selectedItems.remove(index);
        } else {
            Log.v(TAG, "toggleSelection adding selection on position " + position);
            selectedItems.add(position);
        }
        Log.v(TAG, "toggleSelection notifyItemChanged on position " + position);
        notifyItemChanged(position);
        Log.v(TAG, "toggleSelection current selection " + selectedItems);
    }

    /**
     * Change all items status's to selected
     */
    public void selectAll() {
        Log.v(TAG, "selectAll");
        selectedItems = new ArrayList<>(getItemCount());
        for (int i = 0; i < getItemCount(); i++) {
            selectedItems.add(i);
            Log.v(TAG, "selectAll notifyItemChanged on position " + i);
            notifyItemChanged(i);
        }
    }

    /**
     * Clear the selection status for all items one by one and it doesn't stop animations in the items.
     * We use an iterator as we are iterating throught the list and removing items, if this was to be
     * done using the ArrayList a ConcurrentModificationException can be thrown.
     * <p>
     * <b>Note:</b> This method use java.util.Iterator to avoid java.util.ConcurrentModificationException.
     */
    public void clearSelection() {
        Iterator<Integer> iterator = selectedItems.iterator();
        while (iterator.hasNext()) {
            //The notify is done only on items that are currently selected.
            int i = iterator.next();
            iterator.remove();
            Log.v(TAG, "clearSelection notifyItemChanged on position " + i);
            notifyItemChanged(i);
        }
    }

    /**
     * Count the selected items.
     *
     * @return Selected items count
     */
    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    /**
     * Gets the list of selected items.
     *
     * @return List of selected items ids
     */
    public List<Integer> getSelectedItems() {
        return selectedItems;
    }

    /**
     * Save the state of the current selection on the items. Used for orientation change.
     *
     * @param outState Current state
     */
    public void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(TAG, selectedItems);
        outState.putInt(SAVED_INSTANCE_MODE, mode);
    }

    /**
     * Restore the previous state of the selection on the items. Used for orientation change.
     *
     * @param savedInstanceState Previous state
     */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        selectedItems = savedInstanceState.getIntegerArrayList(TAG);
        mode = savedInstanceState.getInt(SAVED_INSTANCE_MODE);
    }
}

