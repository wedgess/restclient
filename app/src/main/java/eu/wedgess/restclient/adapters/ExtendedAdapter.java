package eu.wedgess.restclient.adapters;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import eu.wedgess.restclient.model.RequestData;

/**
 * An extended adapter for the RecyclerViews Adapter.
 * This class extends {@link SelectableAdapter} to offer extra functionality to the RecyclerView.
 * Included Functionality:
 * -- Multi-selection
 * -- Multi-select menu - when actionmode is implemented {@link eu.wedgess.restclient.fragments.BaseStoredDataFragment}
 * -- Undoing deleted items - through SnackBar {@link eu.wedgess.restclient.fragments.BaseStoredDataFragment}
 * <p>
 * Uses generics to allow being extended
 * <p>
 * Created by Gareth on 11/11/2016.
 * Idea based on: https://goo.gl/ICFpyu by: Davide Steduto
 **/

public abstract class ExtendedAdapter<VH extends RecyclerView.ViewHolder> extends SelectableAdapter<VH> {

    private static final String TAG = ExtendedAdapter.class.getSimpleName();
    private static final long UNDO_TIMEOUT = 5000L;

    /**
     * Lock object used to modify the content of {@link #mItems}.
     * Any write operation performed on the list items should be synchronized on this lock.
     */
    private final Object mLock = new Object();

    // set by subclass by calling super(items)
    private List<RequestData> mItems;
    // for keeping track of deleted items
    private List<RequestData> mDeletedItems;
    // keep original positions of deleted items for restoring
    private List<Integer> mOriginalPosition;
    // Handler for sending message when undo timer has run out
    private Handler mHandler;

    /**
     * Callback used for when undo timer has run out and items have been deleted
     * permanently.
     */
    public interface OnDeleteCompleteListener {
        /**
         * Called when snackbar confirmation is dismissed
         */
        void onDeleteConfirmed();
    }

    // subclass must implement their own onCreateViewHolder - using generics to say its a type of ViewHolder
    @Override
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    // subclass must implement their own onBindViewHolder - using generics to say its a type of ViewHolder
    @Override
    public abstract void onBindViewHolder(VH holder, final int position);

    /**
     * Constructor needs the list of RequestData items.
     *
     * @param items - items to set this adapters list
     */
    ExtendedAdapter(@NonNull List<RequestData> items) {
        mItems = items;
    }

    /**
     * Returns the RequestData object "Item" at position.
     *
     * @param position The position of the item in the list
     * @return The custom "Item" object or null if item not found
     */
    RequestData getItem(int position) {
        if (position < 0 || position >= mItems.size()) {
            return null;
        }
        return mItems.get(position);
    }

    /**
     * Get all items for sub class
     * <p>
     *
     * @return List of requestData
     */
    public List<RequestData> getAllItems() {
        return this.mItems;
    }

    /**
     * Set the items used on orientation change in subclass
     * <p>
     *
     * @param items
     */
    public void setItems(List<RequestData> items) {
        this.mItems = items;
    }


    @Override
    public int getItemCount() {
        return mItems != null ? mItems.size() : 0;
    }

    /**
     * Insert given Item at position or Add Item at last position.
     *
     * @param position Position of the item to add
     * @param item     The item to add
     */
    private void addItem(int position, RequestData item) {
        if (position < 0) {
            Log.w(TAG, "Cannot addItem on negative position");
            return;
        }
        //Insert Item
        if (position < mItems.size()) {
            Log.v(TAG, "addItem notifyItemInserted on position " + position);
            synchronized (mLock) {
                mItems.add(position, item);
            }
        } else { //Add Item at the last position
            Log.v(TAG, "addItem notifyItemInserted on last position");
            synchronized (mLock) {
                mItems.add(item);
                position = mItems.size();
            }
        }
        // notify adapter of insertion
        notifyItemInserted(position);
    }

	/* DELETE ITEMS METHODS */

    /**
     * The item is retained in a list for an eventual Undo.
     *
     * @param position The position of item to remove
     * @see #restoreDeletedItems()
     * @see #emptyBin()
     */
    private void removeItem(int position) {
        if (position < 0) {
            Log.w(TAG, "Cannot delete item on negative position");
            return;
        }
        if (position < mItems.size()) {
            Log.v(TAG, "delete item notifyItemRemoved on position " + position);
            synchronized (mLock) {
                saveDeletedItem(position, mItems.remove(position));
            }
            notifyItemRemoved(position);
        } else {
            Log.w(TAG, "delete Item WARNING! Position OutOfBounds! Review the position to remove!");
        }
    }

    /**
     * Every item is stored in a list in case the user wishes to Undo deletion
     *
     * @param selectedPositions List of item positions to remove
     * @see #restoreDeletedItems()
     * @see #emptyBin()
     */
    public void removeItems(List<Integer> selectedPositions) {
        Log.v(TAG, "removeItems reverse Sorting positions --------------");
        // Reverse-sort the list
        Collections.sort(selectedPositions, new Comparator<Integer>() {
            @Override
            public int compare(Integer lhs, Integer rhs) {
                return rhs - lhs;
            }
        });

        // Split the list in ranges
        while (!selectedPositions.isEmpty()) {
            if (selectedPositions.size() == 1) {
                removeItem(selectedPositions.get(0));
                //Align the selection list when removing the item
                selectedPositions.remove(0);
            } else {
                int count = 1;
                while (selectedPositions.size() > count && selectedPositions.get(count).equals(selectedPositions.get(count - 1) - 1)) {
                    ++count;
                }

                if (count == 1) {
                    removeItem(selectedPositions.get(0));
                } else {
                    removeRange(selectedPositions.get(count - 1), count);
                }

                for (int i = 0; i < count; ++i) {
                    selectedPositions.remove(0);
                }
            }
            Log.v(TAG, "removeItems current selection " + getSelectedItems());
        }
    }

    /**
     * Removes a range of items.
     *
     * @param positionStart - range starting at
     * @param itemCount     - how many items to remove
     */
    private void removeRange(int positionStart, int itemCount) {
        Log.v(TAG, "removeRange positionStart=" + positionStart + " itemCount=" + itemCount);
        for (int i = 0; i < itemCount; ++i) {
            synchronized (mLock) {
                saveDeletedItem(positionStart, mItems.remove(positionStart));
            }
        }
        Log.v(TAG, "removeRange notifyItemRangeRemoved");
        // notify adapter of the items removed
        notifyItemRangeRemoved(positionStart, itemCount);
    }

	/* UNDO METHODS */

    /**
     * Save temporary Items in case the user wants to undo deletion
     *
     * @param position The position of the item to retain.
     */
    private void saveDeletedItem(int position, RequestData item) {
        if (mDeletedItems == null) {
            mDeletedItems = new ArrayList<>();
            mOriginalPosition = new ArrayList<>();
        }
        Log.v(TAG, "Recycled " + getItem(position) + " on position=" + position);
        mDeletedItems.add(item);
        mOriginalPosition.add(position);
    }

    /**
     * Gets the list of deleted items, used when onDeleteConfirmed is called to remove items
     * by their ID.
     *
     * @return The list of deleted items
     * @see eu.wedgess.restclient.fragments.BaseStoredDataFragment
     */
    public List<RequestData> getDeletedItems() {
        return mDeletedItems;
    }

    /**
     * Restore items just removed.
     */
    public void restoreDeletedItems() {
        stopUndoTimer();
        //Reverse insert (list was reverse ordered on Delete)
        for (int i = mOriginalPosition.size() - 1; i >= 0; i--) {
            RequestData item = mDeletedItems.get(i);
            addItem(mOriginalPosition.get(i), item);
        }
        emptyBin();
    }

    /**
     * Clean memory from items just removed.
     * <b>Note:</b> This method is automatically called after timer is over and after items restoration.
     */
    private synchronized void emptyBin() {
        if (mDeletedItems != null) {
            mDeletedItems.clear();
            mOriginalPosition.clear();
        }
    }

    // get number of deleted items
    public int getDeletedItemCount() {
        return mDeletedItems.size();
    }

    /**
     * Start Undo timer with custom timeout
     *
     * @param listener Delete listener called after timeout
     * @param timeout  Custom timeout
     */
    public void startUndoTimer(long timeout, final OnDeleteCompleteListener listener) {
        mHandler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            public boolean handleMessage(Message message) {
                // no undo action received so send callback to delete items from DB
                if (listener != null) {
                    listener.onDeleteConfirmed();
                }
                emptyBin();
                return true;
            }
        });
        mHandler.sendMessageDelayed(Message.obtain(mHandler), timeout > 0 ? timeout : UNDO_TIMEOUT);
    }

    /**
     * Stop Undo timer.
     * <br/><b>Note:</b> This method is automatically called in case of restoration.
     */
    private void stopUndoTimer() {
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }
}
