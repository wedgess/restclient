package eu.wedgess.restclient.db;

/**
 * Database Constants for column names, and queries
 * 3 Tables in DB:
 * -- Requests (request_table)
 * -- Request Headers (request_headers_table)
 * -- Request Body Params (request_body_params_table)
 * <p>
 * Created by Gareth on 10/11/2016.
 */
@SuppressWarnings({"static final StringBufferReplaceableBystatic final String", "StringBufferReplaceableByString"})
class DBConstants {

    private DBConstants() {
        // non instantiatable
    }

    // DB
    static final String DATABASE_NAME = "requests_db";
    static final int DATABASE_VERSION = 1;

    // REQUEST TABLE COLUMNS AND TABLE NAME
    static final String TABLE_REQUEST = "requests";
    static final String COLUMN_REQUEST_ID = "id";
    static final String COLUMN_REQUEST_URL = "url";
    static final String COLUMN_REQUEST_METHOD = "method";
    static final String COLUMN_REQUEST_TIMEOUT = "timeout";
    static final String COLUMN_REQUEST_BODY_RAW = "body_raw";
    static final String COLUMN_REQUEST_SAVED = "saved";
    static final String COLUMN_REQUEST_RAN_DATE = "date";

    // CREATE REQUEST TABLE QUERY
    static final String CREATE_TABLE_REQUEST = new StringBuilder("CREATE TABLE ")
            .append(TABLE_REQUEST).append("(")
            .append(COLUMN_REQUEST_ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(COLUMN_REQUEST_URL).append(" TEXT, ")
            .append(COLUMN_REQUEST_METHOD).append(" TEXT, ")
            .append(COLUMN_REQUEST_BODY_RAW).append(" TEXT, ")
            .append(COLUMN_REQUEST_TIMEOUT).append(" INTEGER, ")
            .append(COLUMN_REQUEST_SAVED).append(" INTEGER, ")
            .append(COLUMN_REQUEST_RAN_DATE).append(" DATETIME DEFAULT CURRENT_TIMESTAMP")
            .append(")")
            .toString();

    // REQUEST HEADER COLUMNS AND TABLE NAME
    static final String TABLE_REQUEST_HEADER = "request_headers";
    private static final String COLUMN_REQUEST_HEADER_ID = "id";
    static final String COLUMN_REQUEST_HEADER_REQUEST_ID = "request_id";
    static final String COLUMN_REQUEST_HEADER_KEY = "key";
    static final String COLUMN_REQUEST_HEADER_VALUE = "value";

    // CREATE HEADER TABLE QUERY
    static final String CREATE_TABLE_REQUEST_HEADERS = new StringBuilder("CREATE TABLE ")
            .append(TABLE_REQUEST_HEADER).append("(")
            .append(COLUMN_REQUEST_HEADER_ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(COLUMN_REQUEST_HEADER_REQUEST_ID).append(" INTEGER, ")
            .append(COLUMN_REQUEST_HEADER_KEY).append(" TEXT, ")
            .append(COLUMN_REQUEST_HEADER_VALUE).append(" TEXT, ")
            .append(" FOREIGN KEY (").append(COLUMN_REQUEST_HEADER_REQUEST_ID)
            .append(") REFERENCES ").append(TABLE_REQUEST).append("(")
            .append(COLUMN_REQUEST_HEADER_ID).append("))")
            .toString();

    // REQUEST BODY PARAMS COLUMNS AND TABLE NAME
    static final String TABLE_REQUEST_BODY_PARAMS = "request_body_params";
    private static final String COLUMN_REQUEST_BODY_PARAMS_ID = "id";
    static final String COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID = "request_id";
    static final String COLUMN_REQUEST_BODY_PARAMS_KEY = "key";
    static final String COLUMN_REQUEST_BODY_PARAMS_VALUE = "value";

    // REQUEST BODY PARAMS CREATE TABLE
    static final String CREATE_TABLE_REQUEST_BODY_PARAMS = new StringBuilder("CREATE TABLE ")
            .append(TABLE_REQUEST_BODY_PARAMS).append("(")
            .append(COLUMN_REQUEST_BODY_PARAMS_ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
            .append(COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID).append(" INTEGER, ")
            .append(COLUMN_REQUEST_BODY_PARAMS_KEY).append(" TEXT, ")
            .append(COLUMN_REQUEST_BODY_PARAMS_VALUE).append(" TEXT, ")
            .append(" FOREIGN KEY (").append(COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID)
            .append(") REFERENCES ").append(TABLE_REQUEST).append("(")
            .append(COLUMN_REQUEST_HEADER_ID).append("))")
            .toString();

    // QUERIES //

    // GET REQUEST BY ID QUERY
    static final String QUERY_REQUEST_BY_ID = new StringBuilder()
            .append("SELECT * FROM ")
            .append(TABLE_REQUEST)
            .append(" WHERE ")
            .append(COLUMN_REQUEST_ID)
            .append(" =? ").toString();

    // GET DISTINCT LIST OF URLS QUERY
    static final String QUERY_REQUEST_URLS = new StringBuilder()
            .append("SELECT DISTINCT ")
            .append(COLUMN_REQUEST_URL)
            .append(" FROM ")
            .append(TABLE_REQUEST).toString();

    // GET REQUEST HEADER BY REQUEST ID QUERY
    static final String QUERY_REQUEST_BODY_PARAMS_BY_REQ_ID = new StringBuilder("SELECT  * FROM ")
            .append(TABLE_REQUEST_BODY_PARAMS)
            .append(" WHERE ")
            .append(COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID)
            .append(" =? ")
            .toString();

    // GET REQUEST BODY PARAMS BY REQUEST ID QUERY
    static final String QUERY_REQUEST_HEADER_BY_REQ_ID = new StringBuilder("SELECT  * FROM ")
            .append(TABLE_REQUEST_HEADER)
            .append(" WHERE ")
            .append(COLUMN_REQUEST_HEADER_REQUEST_ID)
            .append(" =? ")
            .toString();
}
