package eu.wedgess.restclient.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import eu.wedgess.restclient.R;
import eu.wedgess.restclient.model.RequestData;
import eu.wedgess.restclient.utils.Utils;

import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_BODY_PARAMS_KEY;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_BODY_PARAMS_VALUE;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_BODY_RAW;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_HEADER_KEY;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_HEADER_REQUEST_ID;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_HEADER_VALUE;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_ID;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_METHOD;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_RAN_DATE;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_SAVED;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_TIMEOUT;
import static eu.wedgess.restclient.db.DBConstants.COLUMN_REQUEST_URL;
import static eu.wedgess.restclient.db.DBConstants.QUERY_REQUEST_BODY_PARAMS_BY_REQ_ID;
import static eu.wedgess.restclient.db.DBConstants.QUERY_REQUEST_BY_ID;
import static eu.wedgess.restclient.db.DBConstants.QUERY_REQUEST_HEADER_BY_REQ_ID;
import static eu.wedgess.restclient.db.DBConstants.QUERY_REQUEST_URLS;
import static eu.wedgess.restclient.db.DBConstants.TABLE_REQUEST;
import static eu.wedgess.restclient.db.DBConstants.TABLE_REQUEST_BODY_PARAMS;
import static eu.wedgess.restclient.db.DBConstants.TABLE_REQUEST_HEADER;

/**
 * DBGateway For all tables
 * Created by Gareth on 13/11/2016.
 */
@SuppressWarnings({"unused", "StringBufferReplaceableByString"})
public class DBUtils {

    private static final String TAG = DBUtils.class.getSimpleName();

    private DBUtils() {
        // non instantiable
    }

    /**
     * Create the Request Row
     *
     * @param requestData - Request to store in DB
     * @param context     - current context
     * @return - rows id
     */
    public static long insertRequestRow(RequestData requestData, Context context) {

        final long id = DBHelper.getInstance(context).getWritableDatabase()
                .insert(TABLE_REQUEST, null, getRequestValues(requestData));
        if (requestData.getHeaders() != null) {
            // add each header to the Request Header Table
            for (LinkedHashMap.Entry<String, String> entry : requestData.getHeaders().entrySet()) {
                insertRequestHeaderRow(id, entry.getKey(), entry.getValue(), context);
            }
        }
        if (requestData.getBodyKeyValuePair() != null) {
            // add each body parameter to the Request Body Params Table
            for (LinkedHashMap.Entry<String, String> entry : requestData.getBodyKeyValuePair().entrySet()) {
                insertRequestBodyParamsRow(id, entry.getKey(), entry.getValue(), context);
            }
        }
        return id;
    }

    /**
     * Creating request header data row.
     *
     * @param id      - requests id
     * @param key     - header key
     * @param value   - header value
     * @param context - current context
     * @return - rows id
     */
    private static synchronized long insertRequestHeaderRow(long id, String key, String value, Context context) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_REQUEST_HEADER_REQUEST_ID, id);
        values.put(COLUMN_REQUEST_HEADER_KEY, key);
        values.put(COLUMN_REQUEST_HEADER_VALUE, value);

        // insert row return ID, returning primary key
        return DBHelper.getInstance(context).getWritableDatabase().insert(TABLE_REQUEST_HEADER, null, values);
    }

    /**
     * Creating request body params data row
     *
     * @param id      - Requests ID
     * @param key     - Body Param Key
     * @param value   - Body Param Value
     * @param context - Current context
     * @return - rows id
     */
    private static synchronized long insertRequestBodyParamsRow(long id, String key, String value, Context context) {

        ContentValues values = new ContentValues();
        values.put(COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID, id);
        values.put(COLUMN_REQUEST_BODY_PARAMS_KEY, key);
        values.put(COLUMN_REQUEST_BODY_PARAMS_VALUE, value);

        // insert row return ID, returning primary key
        return DBHelper.getInstance(context).getWritableDatabase().insert(TABLE_REQUEST_BODY_PARAMS, null, values);
    }

    /**
     * Get Request row by ID
     *
     * @param id      - id of RequestData row to fetch
     * @param context - current context
     * @return - the RequestData
     */
    public static RequestData getRequestRowById(long id, Context context) {
        Cursor cursor = DBHelper.getInstance(context).getReadableDatabase()
                .rawQuery(QUERY_REQUEST_BY_ID, new String[]{String.valueOf(id)});

        RequestData requestData = null;
        if (cursor != null) {
            cursor.moveToFirst();
            try {
                requestData = buildRequestFromCursor(cursor, id, context);
            } catch (CursorIndexOutOfBoundsException e) {
                Log.e(TAG, "Error retrieving row by id: " + id, e);
            } finally {
                cursor.close();
            }
        }
        return requestData;
    }

    /**
     * Get all requests stored in the DB
     *
     * @param isSavedData - if the item is saved entry or not
     * @param context     - current context
     * @return ArrayList of RequestData
     */
    public static ArrayList<RequestData> getAllRequests(boolean isSavedData, Context context) {
        ArrayList<RequestData> requestPackages = new ArrayList<>();
        // needs to be here rather than DBConstants as isSavedData can vary
        String selectQuery = new StringBuilder()
                .append("SELECT  * FROM ")
                .append(TABLE_REQUEST)
                .append(" WHERE ")
                .append(COLUMN_REQUEST_SAVED)
                .append(" = ")
                .append(isSavedData ? "1" : "0")
                .append(" ORDER BY datetime(")
                .append(COLUMN_REQUEST_RAN_DATE)
                .append(") DESC")
                .toString();
        Cursor cursor = DBHelper.getInstance(context).getReadableDatabase().rawQuery(selectQuery, null);

        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    RequestData requestData = new RequestData();
                    try {
                        final long id = cursor.getLong(cursor.getColumnIndex(COLUMN_REQUEST_ID));
                        requestPackages.add(buildRequestFromCursor(cursor, id, context));
                    } catch (CursorIndexOutOfBoundsException e) {
                        Log.e(TAG, "Error fetching all request data", e);
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return requestPackages;
    }

    /**
     * Get all request urls, for autocomplete.
     *
     * @param context - current context
     * @return - List of URL Strings
     */
    public static ArrayList<String> getAllRequestUrls(Context context) {
        ArrayList<String> urls = new ArrayList<>();
        Cursor cursor = DBHelper.getInstance(context).getReadableDatabase().rawQuery(QUERY_REQUEST_URLS, null);

        // add http:// and https:// as first items in autocomplete textview
        urls.add(context.getString(R.string.http));
        urls.add(context.getString(R.string.https));

        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    urls.add(cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_URL)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return urls;
    }

    /**
     * Get request header data for request id
     * Use a LinkedHashMap to maintain the order of items.
     *
     * @param id      - id of regex
     * @param context - current context
     * @return - LinkedHashMap of the Request Headers
     */
    private static LinkedHashMap<String, String> getHeaderDataByRequestId(long id, Context context) {
        LinkedHashMap<String, String> headerData = new LinkedHashMap<>();
        Cursor cursor = DBHelper.getInstance(context).getReadableDatabase()
                .rawQuery(QUERY_REQUEST_HEADER_BY_REQ_ID, new String[]{String.valueOf(id)});

        if (cursor != null) {
            // looping through all rows and adding to LinkedHashMap
            if (cursor.moveToFirst()) {
                do {
                    headerData.put(cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_HEADER_KEY)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_HEADER_VALUE)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return headerData;
    }

    /**
     * Get request body params data for request id
     * Use a LinkedHashMap to keep maintain the order of insertion.
     *
     * @param id      - RequestDatas id
     * @param context - current context
     * @return - LinkedHashMap of request header data
     */
    private static LinkedHashMap<String, String> getBodyParamDataByRequestId(long id, Context context) {
        LinkedHashMap<String, String> bodyParamData = new LinkedHashMap<>();
        Cursor cursor = DBHelper.getInstance(context).getReadableDatabase()
                .rawQuery(QUERY_REQUEST_BODY_PARAMS_BY_REQ_ID, new String[]{String.valueOf(id)});

        if (cursor != null) {
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    bodyParamData.put(cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_BODY_PARAMS_KEY)),
                            cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_BODY_PARAMS_VALUE)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        return bodyParamData;
    }

    /**
     * Deleting a request row by id and associated rows in Request Header and Request Body params table
     *
     * @param id      - RequestDatas ID
     * @param context - current context
     */
    public static synchronized void deleteRequestById(long id, Context context) {
        DBHelper.getInstance(context).getWritableDatabase()
                .delete(TABLE_REQUEST, COLUMN_REQUEST_ID + " =?",
                        new String[]{String.valueOf(id)});
        deleteRequestHeaderById(id, context);
        deleteRequestBodyParamById(id, context);
    }

    /**
     * Deletes request header data by request ID
     *
     * @param id      - id of Request Header row to delete
     * @param context - current context
     */
    private static void deleteRequestHeaderById(long id, Context context) {
        // delete request header data associated with this entry
        DBHelper.getInstance(context).getWritableDatabase()
                .delete(TABLE_REQUEST_HEADER, COLUMN_REQUEST_HEADER_REQUEST_ID + " =?",
                        new String[]{String.valueOf(id)});
    }

    /**
     * Deletes body param keys by request ID
     *
     * @param id      - id of Request Body row to delete
     * @param context - current context
     */
    private static void deleteRequestBodyParamById(long id, Context context) {
        // delete request body params data associated with this entry
        DBHelper.getInstance(context).getWritableDatabase()
                .delete(TABLE_REQUEST_BODY_PARAMS, COLUMN_REQUEST_BODY_PARAMS_REQUEST_ID + " =?",
                        new String[]{String.valueOf(id)});
    }

    /**
     * Updating a request row.
     * Rows associated with the RequestData need to be removed such as
     * HeaderData rows and body param rows. Then re-add the new rows Request TABLE can
     * be updated as normal.
     *
     * @param requestData - RequestData to update
     * @param context     - current context
     */
    public static void updateRequestRow(RequestData requestData, Context context) {
        // remove all old header data
        deleteRequestHeaderById(requestData.getId(), context);
        // add the newly updated header data
        for (LinkedHashMap.Entry<String, String> entry : requestData.getHeaders().entrySet()) {
            insertRequestHeaderRow(requestData.getId(), entry.getKey(), entry.getValue(), context);
        }
        // remove all old body params
        deleteRequestBodyParamById(requestData.getId(), context);
        // add each body parameter to the Request Body Params Table
        for (LinkedHashMap.Entry<String, String> entry : requestData.getBodyKeyValuePair().entrySet()) {
            insertRequestBodyParamsRow(requestData.getId(), entry.getKey(), entry.getValue(), context);
        }

        // updating row
        DBHelper.getInstance(context).getWritableDatabase()
                .update(TABLE_REQUEST, getRequestValues(requestData), COLUMN_REQUEST_ID + " =?",
                        new String[]{String.valueOf(requestData.getId())});
    }


    /**
     * Builds contents values for RequestData TABLE
     *
     * @param requestData - RequestData to get ContentValue object for
     * @return ContentValues
     */
    private static ContentValues getRequestValues(RequestData requestData) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_REQUEST_URL, requestData.getUrl());
        values.put(COLUMN_REQUEST_METHOD, requestData.getMethod());
        values.put(COLUMN_REQUEST_BODY_RAW, requestData.getBodyRaw());
        values.put(COLUMN_REQUEST_TIMEOUT, requestData.getTimeOut());
        values.put(COLUMN_REQUEST_SAVED, requestData.isSaved() ? 1 : 0);
        return values;
    }

    /**
     * Build RequestData from cursor object
     *
     * @param cursor  - the cursor
     * @param id      - RequestDatas ID
     * @param context - current context
     * @return - RequestData
     * @throws CursorIndexOutOfBoundsException
     */
    private static RequestData buildRequestFromCursor(Cursor cursor, long id, Context context)
            throws CursorIndexOutOfBoundsException {
        RequestData requestData = new RequestData();
        // use id which was passed in
        requestData.setId(id);
        requestData.setUrl((cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_URL))));
        requestData.setMethod((cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_METHOD))));
        requestData.setBodyRaw((cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_BODY_RAW))));
        String dateString = cursor.getString(cursor.getColumnIndex(COLUMN_REQUEST_RAN_DATE));
        requestData.setCreatedAt(Utils.stringToDate(dateString));
        requestData.setSaved(cursor.getInt(cursor.getColumnIndex(COLUMN_REQUEST_SAVED)) == 1);
        requestData.setTimeOut((cursor.getInt(cursor.getColumnIndex(COLUMN_REQUEST_TIMEOUT))));
        requestData.setHeaders(getHeaderDataByRequestId(id, context));
        requestData.setBodyKeyValuePair(getBodyParamDataByRequestId(id, context));
        return requestData;
    }
}
