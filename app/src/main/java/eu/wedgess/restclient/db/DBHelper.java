package eu.wedgess.restclient.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static eu.wedgess.restclient.db.DBConstants.CREATE_TABLE_REQUEST;
import static eu.wedgess.restclient.db.DBConstants.CREATE_TABLE_REQUEST_BODY_PARAMS;
import static eu.wedgess.restclient.db.DBConstants.CREATE_TABLE_REQUEST_HEADERS;
import static eu.wedgess.restclient.db.DBConstants.DATABASE_NAME;
import static eu.wedgess.restclient.db.DBConstants.DATABASE_VERSION;

/**
 * Helper class for DB, takes care of creating and updating DB.
 * It is a singleton.
 * <p>
 * Created by Gareth on 10/11/2016.
 */
@SuppressWarnings("StringBufferReplaceableByString")
class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = DBHelper.class.getSimpleName();
    private static DBHelper mInstance;

    static synchronized DBHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new DBHelper(context.getApplicationContext());
        }
        return mInstance;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating apps SQLite tables
        db.execSQL(CREATE_TABLE_REQUEST);
        db.execSQL(CREATE_TABLE_REQUEST_HEADERS);
        db.execSQL(CREATE_TABLE_REQUEST_BODY_PARAMS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);
    }
}
