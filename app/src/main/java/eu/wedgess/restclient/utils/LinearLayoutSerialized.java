package eu.wedgess.restclient.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import java.io.Serializable;

/**
 * Needed to store LinearLayouts in a Map. This was fine until I tried restoring them on Orientation change
 * received RunTimeException due to LinearLayouts not being serialized but Map is.
 * <p>
 * java.lang.RuntimeException: Parcel: unable to marshal value android.widget.LinearLayout
 * <p>
 * The solution is to extend {@link LinearLayout} and implement {@link Serializable}
 * Created by Gareth on 14/11/2016.
 */

public class LinearLayoutSerialized extends LinearLayout implements Serializable {
    public LinearLayoutSerialized(Context context) {
        super(context);
    }

    public LinearLayoutSerialized(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LinearLayoutSerialized(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public LinearLayoutSerialized() {
        super(null);
    }
}
