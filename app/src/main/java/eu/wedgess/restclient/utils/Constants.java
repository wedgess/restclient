package eu.wedgess.restclient.utils;

/**
 * Application Constants.
 * <p>
 * Created by Gareth on 10/11/2016.
 */

public class Constants {

    private Constants() {
        // non instantiatable
    }

    // INTENT FOR PASSING REQUEST POSITION AND IF SAVED OR NOT
    public static final String INTENT_EXTRA_PASSED_REQUEST = "passedRequestKey";

    // SAVED INSTANCE STATE KEYS
    public static final String SAVED_STATE_REQUEST_BODY_LAYOUTS = "bodyLayoutsKey";
    public static final String SAVED_STATE_REQUEST_BODY_KEY_VALUES = "bodyKeyValKey";
    public static final String SAVED_STATE_REQUEST_TIMEOUT = "timeoutKey";
    public static final String SAVED_STATE_REQUEST_HEADER_LAYOUTS = "headerLayoutsKey";
    public static final String SAVED_STATE_REQUEST_HEADER_KEY_VALUES = "headerKeyValKey";
    public static final String SAVED_STATE_BODY_REQUEST_RAW = "bodyRawKey";
    public static final String SAVED_STATE_REQUEST_URL = "urlKey";
    public static final String SAVED_STATE_REQUEST_METHOD = "methodKey";
    public static final String SAVED_STATE_IS_BODY_EXPANDED = "bodyExpandedKey";
    public static final String SAVED_STATE_IS_HEADERS_EXPANDED = "headersExpandedKey";
    public static final String SAVED_STATE_BODY_RAW_IS_CHECKED = "rawRadioKey";
    public static final String SAVED_STATE_RESPONSE_IS_HEADERS_EXPANDED = "responseHeadersExpandedKey";
    public static final String SAVED_STATE_RESPONSE_IS_BODY_EXPANDED = "responseBodyExpandedKey";
    public static final String SAVED_STATE_RESPONSE_SCROLL_POS = "responseScrollPos";
    public static final String SAVED_STATE_RESPONSE_AVAIL = "responseAvailableKey";
    public static final String SAVED_STATE_RESPONSE_DATA = "responseDataKey";

    // GLOBAL INVALID POSITION
    public static final int INVALID_POSITION = -1;

    // REQUEST METHOD TYPES
    public static final String REQUEST_METHOD_POST = "POST";
    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_OPTIONS = "OPTIONS";
    public static final String REQUEST_METHOD_PUT = "PUT";
    public static final String REQUEST_METHOD_PATCH = "PATCH";
    public static final String REQUEST_METHOD_DELETE = "DELETE";
}
