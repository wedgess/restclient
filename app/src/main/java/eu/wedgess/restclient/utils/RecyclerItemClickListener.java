package eu.wedgess.restclient.utils;

/**
 * Simple click callback for normal click and long click on Recycler items.
 * Used by {@link eu.wedgess.restclient.adapters.StoredDataAdapter}.
 * implemented by {@link eu.wedgess.restclient.fragments.BaseStoredDataFragment}
 * <p>
 * Created by Gareth on 15/11/2016.
 */

public interface RecyclerItemClickListener {
    void onItemClick(int position);

    void onItemLongClick(int position);
}
