package eu.wedgess.restclient.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.LinearLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import eu.wedgess.restclient.R;

/**
 * Random utility methods used across application.
 * <p>
 * Created by Gareth on 12/11/2016.
 */

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    private Utils() {
        // non-instantiatable
    }

    /**
     * Converts a string date to a date object
     * This method when pulling items from DB and creating RequestData objects
     *
     * @param dateString - date as a string
     * @return Date object
     */
    public static Date stringToDate(String dateString) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            Log.e(TAG, "Parsing date failed", e);
            return null;
        }
    }

    /**
     * This method gets the date as Today, Yesterday, 2 days ago..., Nov 4th etc
     *
     * @param date - date relative to now
     * @return -- abbreviated date
     */
    public static CharSequence getRelativeDate(Date date) {
        // TESTING: see what it looks like when the date is 6 days ago
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.DATE, -6);
        //Log.i(TAG, DateUtils.getRelativeTimeSpanString(cal.getTime().getTime(), new Date().getTime(), 0L, DateUtils.FORMAT_ABBREV_ALL).toString());
        String relativeDate = DateUtils.getRelativeTimeSpanString(date.getTime(), new Date().getTime(), 0L, DateUtils.FORMAT_ABBREV_ALL).toString();
        // these could go into donottranslate.xml however hardcoded for now
        if (relativeDate.contains("min") || relativeDate.contains("hr") || relativeDate.contains("sec")) {
            relativeDate = "Today";
        }

        return relativeDate;
    }

    /**
     * Checks whether the user input enetered in TIL
     * is empty or not, if empty the error is enabled otherwise
     * if error was present and is no longer then it is removed.
     *
     * @param til      - textinputLayout of field which needs validating
     * @param errorMsg - message to display incase of error
     * @return if input is valid
     */
    public static boolean isValidEntry(TextInputLayout til, String errorMsg) {
        if (TextUtils.isEmpty(til.getEditText().getText())) {
            if (!til.isErrorEnabled()) {
                til.setError(errorMsg);
                til.setErrorEnabled(true);
            }
            return false;
        } else {
            if (til.isErrorEnabled()) {
                til.setErrorEnabled(false);
            }
            return true;
        }
    }


    /**
     * Checks whether or nor device is connected to the internet, doesn't
     * matter if it is wifi or data.
     *
     * @param context - the current context
     * @return - true if connected, false if no connection
     */
    public static boolean hasInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * Build a LinkedHashMap from a LinearLayoutSerialized
     * Used by {@link eu.wedgess.restclient.activities.RequestActivity}
     *
     * @param layouts
     * @return
     */
    public static LinkedHashMap<String, String> buildMapFromLayout(final List<LinearLayoutSerialized> layouts) {
        // loop through the values in the layouts, getting the keys and values
        TextInputLayout tilKey, tilValue;
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        for (LinearLayout layout : layouts) {
            tilKey = (TextInputLayout) layout.findViewById(R.id.til_key);
            tilValue = (TextInputLayout) layout.findViewById(R.id.til_value);
            // only add them to the map if both key and value are not empty
            if (!TextUtils.isEmpty(tilKey.getEditText().getText())
                    && !TextUtils.isEmpty(tilValue.getEditText().getText())) {
                map.put(tilKey.getEditText().getText().toString(),
                        tilValue.getEditText().getText().toString());
            }

        }
        return map;
    }

    /**
     * Converts a list of strings to a comma delimited string
     *
     * @param list - list of strings to delimit
     * @return list as a delimited string
     */
    public static String listToDelimitedString(List<String> list) {
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s).append(',');
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    /**
     * Get the selectableItemBackground programmatically so that the recycler items give
     * touch feedback. Such as ripple effect, setting an items background as R.color.normal_bg
     * or and other color will override touch feedback.
     *
     * @param context - the current context
     * @return - resource id for attribute
     */
    public static int getSelectedItemBgAttr(Context context) {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int resId = typedArray.getResourceId(0, 0);
        typedArray.recycle();
        return resId;
    }
}
