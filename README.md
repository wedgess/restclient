# Android Application - Using SQLite DB #
----------------------------------------------------

### Brief ###
Development of an Android App that Creates, Reads, Updates and Deletes from an SQLite database. Also make use of fragments.

### My Project ###

I created a REST client for testing my third year projects (Chitter) REST API.

# How my project looks? #
--------------------------------------

![Alt text](http://i.imgur.com/ubq0oKe.png "Request History Screen")

![Alt text](http://i.imgur.com/tJnazzN.png "Request Screen")

![Alt text](http://i.imgur.com/BTSjLCA.png "Response Dialog")